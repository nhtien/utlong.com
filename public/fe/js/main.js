$('select.js-choose-province').change(function (event) {
    const optionSelected = $("select.js-choose-province > option:selected", this);
    const provinceId = this.value;

    getDistrictByProvince(provinceId);

    // ward
    $('select.js-choose-ward').empty();
});

$('select.js-choose-district').change(function (event) {
    const optionSelected = $("select.js-choose-district > option:selected", this);
    const districtId = this.value;

    getWardByDistrict(districtId);
});

function getDistrictByProvince(provinceID) {
    const districtElem = $('select.js-choose-district');
    districtElem.empty();

    $.ajax({
        url: "/district/" + provinceID + "/danh-sach.html",
    }).done(function(res) {
        if (res.length > 0) {
            let optElems = '<option value="0">-- Chọn Quận / Huyện --</option>';
            $.each(res, function( index, value ) {
                optElems += "<option value='" + value.id + "'>" + value.name + "</option>";
            });
            districtElem.html(optElems);
        }
    });
}

function getWardByDistrict(districtID) {
    const wardElem = $('select.js-choose-ward');
    wardElem.empty();

    $.ajax({
        url: "/ward/" + districtID + "/danh-sach.html",
    }).done(function(res) {
        if (res.length > 0) {
            let optElems = '<option value="0">-- Chọn Phường / Xã --</option>';
            $.each(res, function( index, value ) {
                optElems += "<option value='" + value.id + "'>" + value.name + "</option>";
            });
            wardElem.html(optElems);
        }
    });
}
