<?php
use App\Http\Controllers\BE\Auth\LoginController;
use App\Http\Controllers\BE\Auth\LogoutController;
use App\Http\Controllers\BE\CkfinderController;
use App\Http\Controllers\BE\ConfigController;
use App\Http\Controllers\BE\IndexController;
use App\Http\Controllers\BE\MenuController;
use App\Http\Controllers\BE\MenuFrontendController;
use App\Http\Controllers\BE\ModelLogController;
use App\Http\Controllers\BE\TestingController;
use App\Http\Controllers\BE\UserController;
use App\Http\Controllers\BE\UserGroupController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Route(s) - FE
|--------------------------------------------------------------------------
*/
Route::get('/', function () {
//    if (in_array(request()->getHttpHost(), ['www.utlong.com', 'utlong.com',  'utlong.local'])) {
//        return to_route('farmers.homepage');
//    }
    if (in_array(request()->getHttpHost(), ['www.utlong.com', 'utlong.com'])) {
        return to_route('farmers.homepage');
    }
    return to_route('machine.homepage');
})->name('home');

/*
|--------------------------------------------------------------------------
| START - FARMERS
|--------------------------------------------------------------------------
*/
Route::controller(\App\Http\Controllers\FE\Farmers\LoginController::class)->group(function () {
    Route::get('/nong-dan/dang-nhap.html',  'loginForm')->name( 'farmer.login');
    Route::post('/nong-dan/dang-nhap.html', 'login')->middleware('throttle:30,5');
});

Route::controller(\App\Http\Controllers\FE\Farmers\RegisterController::class)->group(function () {
    Route::get('/nong-dan/dang-ky.html',  'registerForm')->name( 'farmer.form-register')->middleware('throttle:30,5');
    Route::post('/nong-dan/dang-ky.html', 'register')->name( 'farmer.register')->middleware('throttle:30,5');
});

Route::controller(\App\Http\Controllers\FE\Machine\RegisterController::class)->group(function () {
    Route::get('/chu-may/dang-ky.html',  'registerForm')->name( 'machine.form-register')->middleware('throttle:30,5');
    Route::post('/chu-may/dang-ky.html', 'register')->name( 'machine.register')->middleware('throttle:30,5');
});

Route::controller(\App\Http\Controllers\FE\Machine\LoginController::class)->group(function () {
    Route::get('/chu-may/dang-nhap.html',  'loginForm')->name( 'machine.login');
    Route::post('/chu-may/dang-nhap.html', 'login')->middleware('throttle:30,5');
});

Route::controller(\App\Http\Controllers\FE\Farmers\LogoutController::class)->group(function () {
    Route::get('/nong-dan/dang-xuat.html', 'logout')->name('farmer.logout');
/*    Route::get('/logout-other-devices.html', 'logoutForm')->name('farmer.logout-other-devices');
    Route::post('/logout-other-devices.html', 'logoutOtherDevices');*/
});

Route::controller(\App\Http\Controllers\FE\Machine\LogoutController::class)->group(function () {
    Route::get('/chu-may/dang-xuat.html', 'logout')->name('machine.logout');
});

Route::controller(\App\Http\Controllers\FE\DistrictController::class)->group(function () {
    Route::get('/district/{province_id}/danh-sach.html', 'findsDistrictByProvince');
});

Route::controller(\App\Http\Controllers\FE\WardController::class)->group(function () {
    Route::get('/ward/{district_id}/danh-sach.html', 'findsWardByDistrict');
});

Route::middleware(['auth:farmer'])->group(function () {
    Route::controller(\App\Http\Controllers\FE\Farmers\IndexController::class)->group(function () {
        Route::get('/nong-dan/trang-chu.html',  'index')->name( 'farmers.homepage');
    });
    Route::controller(\App\Http\Controllers\FE\Farmers\FarmerController::class)->group(function () {
        Route::get('/nong-dan/danh-sach-lich.html',  'index')->name( 'farmer.index');
        Route::get('/nong-dan/cap-nhat-lich-dat-{id}.html',  'edit')->name( 'farmer.edit');
        Route::post('/nong-dan/cap-nhat-lich-dat.html',  'update')->name( 'farmer.update');
        Route::get('/nong-dan/dat-lich.html',  'create')->name( 'farmer.create');
        Route::post('/nong-dan/dat-lich.html',  'store')->name( 'farmer.store');
    });
});

Route::middleware(['auth:machine'])->group(function () {
    Route::controller(\App\Http\Controllers\FE\Machine\IndexController::class)->group(function () {
        Route::get('/chu-may/trang-chu.html',  'index')->name( 'machine.homepage');
    });
    Route::controller(\App\Http\Controllers\FE\Machine\MachineController::class)->group(function () {
        Route::get('/chu-may/danh-sach-lich.html',  'index')->name( 'machine.index');
        Route::get('/chu-may/cap-nhat-lich-dat-{id}.html',  'show')->name( 'machine.show');
        Route::get('/chu-may/xac-nhan-da-thuc-hien-{id}.html',  'confirmStatus')->name( 'machine.confirm-status');
        Route::get('/chu-may/xac-nhan-da-thanh-toan-{id}.html',  'confirmPaymentStatus')->name( 'machine.confirm-payment-status');
        Route::get('/chu-may/dat-lich.html',  'create')->name( 'machine.create');
        Route::post('/chu-may/dat-lich.html',  'store')->name( 'machine.store');
    });
});



/*
|--------------------------------------------------------------------------
| END - FARMERS
|--------------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------------
| Login/Logout Route(s) - BE
|--------------------------------------------------------------------------
*/
Route::controller(LoginController::class)->group(function () {
    Route::get('/login.html',  'loginForm')->name( 'login')->middleware('throttle:10,5');
    Route::post('/login.html', 'login')->middleware('throttle:10,5');
});

Route::controller(LogoutController::class)->group(function () {
    Route::get('/logout.html', 'logout')->name('logout');
    Route::get('/logout-other-devices.html', 'logoutForm')->name('logout-other-devices');
    Route::post('/logout-other-devices.html', 'logoutOtherDevices');
});

/*
|--------------------------------------------------------------------------
| BE
|--------------------------------------------------------------------------
*/
Route::get('/utlong_admin.html',  [IndexController::class, 'dashboard'])->name('dashboard-backend-2')->middleware('auth');
Route::middleware(['auth'])->group(function () {
    $backend = config('constants.route.backend');

    Route::prefix($backend)->group(function (){
        Route::controller(IndexController::class)->group(function (){
            Route::get('/dashboard.html',  'dashboard')->name( "dashboard-backend");
            Route::get('/index/cache',  'cache')->name( "be.index.cache");
        });
    });

    Route::prefix($backend)->group(function (){
        Route::controller(UserController::class)->group(function (){
            Route::get('/users/index',  'index')->name( "be.users.index");
            Route::get('/users/create',  'create')->name( "be.users.create");
            Route::get('/users/edit',  'edit')->name( "be.users.edit");
            Route::put('/users/update',  'update')->name( "be.users.update");
            Route::post('/users/store',  'store')->name( "be.users.store");
            Route::delete('/users/destroy',  'destroy')->name( "be.users.destroy");
            Route::get('/users/edit-profile',  'editProfile')->name( "be.users.edit-profile");
            Route::put('/users/update-profile',  'updateProfile')->name( "be.users.update-profile");
        });
    });

    Route::prefix($backend)->group(function (){
        Route::controller(ModelLogController::class)->group(function (){
            Route::get('/model-log/index',  'index')->name( "be.modellog.index");
            Route::get('/model-log/show',  'show')->name( "be.modellog.show");
        });
    });

    Route::prefix($backend)->group(function (){
        Route::controller(ConfigController::class)->group(function (){
            Route::get('/config/index',  'index')->name( "be.config.index");
            Route::get('/config/create',  'create')->name( "be.config.create");
            Route::get('/config/edit',  'edit')->name( "be.config.edit");
            Route::put('/config/update',  'update')->name( "be.config.update");
            Route::post('/config/store',  'store')->name( "be.config.store");
            Route::delete('/config/destroy',  'destroy')->name( "be.config.destroy");
        });
    });

    Route::prefix($backend)->group(function (){
        Route::controller(MenuController::class)->group(function (){
            Route::get('/menu/index',  'index')->name( "be.menu.index");
            Route::get('/menu/create',  'create')->name( "be.menu.create");
            Route::get('/menu/edit',  'edit')->name( "be.menu.edit");
            Route::put('/menu/update',  'update')->name( "be.menu.update");
            Route::post('/menu/store',  'store')->name( "be.menu.store");
            //Route::delete('/menu/destroy',  'destroy')->name( "be.menu.destroy");
        });
    });

    Route::prefix($backend)->group(function (){
        Route::controller(UserGroupController::class)->group(function (){
            Route::get('/user-group/index',  'index')->name( "be.usergroup.index");
            Route::get('/user-group/create',  'create')->name( "be.usergroup.create");
            Route::get('/user-group/edit',  'edit')->name( "be.usergroup.edit");
            Route::put('/user-group/update',  'update')->name( "be.usergroup.update");
            Route::post('/user-group/store',  'store')->name( "be.usergroup.store");
            //Route::delete('/user-group/destroy',  'destroy')->name( "be.usergroup.destroy");
        });
    });

    Route::prefix($backend)->group(function (){
        Route::controller(MenuFrontendController::class)->group(function (){
            Route::get('/menu-frontend/index',  'index')->name( "be.menufrontend.index");
            Route::get('/menu-frontend/create',  'create')->name( "be.menufrontend.create");
            Route::get('/menu-frontend/edit',  'edit')->name( "be.menufrontend.edit");
            Route::put('/menu-frontend/update',  'update')->name( "be.menufrontend.update");
            Route::post('/menu-frontend/store',  'store')->name( "be.menufrontend.store");
            Route::delete('/menu-frontend/destroy',  'destroy')->name( "be.menufrontend.destroy");
        });
    });

    // Testing
    Route::prefix($backend)->group(function (){
        Route::controller(TestingController::class)->group(function (){
            Route::get('/testing/send-mail',  'sendMail')->name( "be.testing.send-mail");
        });
    });

    Route::controller(CkfinderController::class)->group(function (){
        Route::get('/ckfinder-show.html',  'show')->name( "be.ckfinder.show");
    });

});


/*
|--------------------------------------------------------------------------
| System Cache
|--------------------------------------------------------------------------
*/
Route::prefix('systems')->group(function () {

    Route::get('/optimize-cache', function() {
        Artisan::call('optimize');
        return '<h1>Reoptimized class loader</h1>';
    });

    Route::get('/route-cache', function() {
        Artisan::call('route:cache');
        return '<h1>Routes cached</h1>';
    });

    Route::get('/config-cache', function() {
        Artisan::call('config:cache');
        return '<h1>Config cached</h1>';
    });

    Route::get('/optimize-clear', function() {
        Artisan::call('optimize:clear');
        return '<h1>optimize:clear</h1>';
    });

    Route::get('/config-clear', function() {
        Artisan::call('config:clear');
        return '<h1>Configuration cache cleared!</h1>';
    });

    Route::get('/clear-cache', function() {
        Artisan::call('cache:clear');
        return '<h1>Cache facade value cleared</h1>';
    });

    Route::get('/route-clear', function() {
        Artisan::call('route:clear');
        return '<h1>Route cache cleared</h1>';
    });

    Route::get('/view-clear', function() {
        Artisan::call('view:clear');
        return '<h1>View cache cleared</h1>';
    });
});




