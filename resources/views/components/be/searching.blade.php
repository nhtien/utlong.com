<section id="options">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{$title}}</h4>
        </div>
        <div class="card-body">
            <div class="users-list-filter px-1">
                <form method="Get" action="{{$formAction}}" id="main-searching">
                    <div class="row border rounded py-2 mb-2">
                        {{$slot}}
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
