<div class="col-12 col-sm-6 col-lg-3 d-flex align-items-center">
    <button type="submit" class="btn btn-primary btn-block glow users-list-clear">Tìm kiếm</button>
    &nbsp;
    <a href="{{$refreshUrl}}" type="reset" style="margin-top: 0px" class="btn btn-danger btn-block glow users-list-clear mb-0">Làm mới</a>
</div>
