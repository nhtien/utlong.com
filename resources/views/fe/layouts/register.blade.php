<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Đăng ký tài khoản</title>

    <!-- Icons font CSS-->
    <link href="{{asset('/public/fe/register/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('/public/fe/register/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="{{asset('/public/fe/register/vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('/public/fe/register/vendor/datepicker/daterangepicker.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('/public/fe/register/css/main.css')}}" rel="stylesheet" media="all">
</head>

<body>
<div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins" style="background-image: url({{asset('/public/fe/register/images/T20-scaled.jpg')}})">
    <div class="wrapper wrapper--w680">
        <div class="card card-4">
            <div class="card-body" style="padding-top: 15px">
                <div class="wrap">
                    <div>
                        <a href="/"><img style="  display: block;margin-left: auto;margin-right: auto;" src="/public/fe/images/logo/logo.png" alt="logo" width="90px"></a>
                    </div>
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- Jquery JS-->
<script src="{{asset('/public/fe/register/vendor/jquery/jquery.min.js')}}"></script>
<!-- Vendor JS-->
<script src="{{asset('/public/fe/register/vendor/select2/select2.min.js')}}"></script>
<script src="{{asset('/public/fe/register/vendor/datepicker/moment.min.js')}}"></script>
<script src="{{asset('/public/fe/register/vendor/datepicker/daterangepicker.js')}}"></script>

<!-- Main JS-->
<script src="{{asset('/public/fe/register/js/global.js')}}"></script>
<script src="{{asset('/public/fe/js/main.js')}}"></script>
<script>
/*    $("button[type='submit']").click(function () {
        $(this).prop('disabled', true);
        $(this).parent().css("display", 'none');
    });*/
</script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->
