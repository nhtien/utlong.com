<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Utlong</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">
    <link href="{{asset('/public/uploads/images/favicon/favicon_48x48.ico')}}" rel="shortcut icon">
    <link href="{{asset('/public/uploads/images/favicon/favicon_192x192.png')}}" rel="icon" sizes="192x192" type="image/png">
    <link href="{{asset('/public/uploads/images/favicon/favicon_96x96.png')}}" rel="icon" sizes="96x96" type="image/png">
    <link href="{{asset('/public/uploads/images/favicon/favicon_48x48.png')}}" rel="icon" sizes="48x48" type="image/png">
    @include('be.layouts.blocks.css')
</head>
<!-- END: Head-->
<!-- BEGIN: Body-->
<body style="position: relative" class="vertical-layout vertical-menu-modern boxicon-layout no-card-shadow 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
{{--overlay--}}
<div class="overlay"></div>
<div class="spanner">
    <div class="loader"></div>
    <p>Đang được xử lý, vui lòng đợi ...</p>
</div>
<form id="delete-form" action="" method="post">
    @csrf @method('DELETE')
</form>
{{--menu--}}
<div class="header-navbar-shadow"></div>
<nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top ">
    <div class="navbar-wrapper">
        <div class="navbar-container content" style="padding-left: 1rem !important;">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu d-xl-none mr-auto"><a href="/"><img style="  display: block;margin-left: auto;margin-right: auto;" src="/public/fe/images/logo/logo.png" alt="logo" width="75px"></a></li>
                    </ul>
                </div>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="javascript:void(0);" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex d-none">
                                <span class="user-name">@auth() {{auth('farmer')->user()->full_name}} @endauth @guest User not found @endguest</span>
                                {{--<span class="user-status text-muted">{{auth('farmer')->user()->full_name}}</span>--}}
                            </div>
                            <span>
                                <img class="round" src="{{asset('public/be/images/man-icon.png')}}" alt="avatar" height="40" width="40">
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pb-0">
{{--                            <a class="dropdown-item" href="{{route('be.users.edit-profile')}}">
                                <i class="bx bx-user mr-50"></i> Edit Profile
                            </a>
                            <div class="dropdown-divider mb-0"></div>--}}
                            <a class="dropdown-item" href="#">
                                <i class="bx bx-money mr-50"></i>Tiền KM: {{currency_format(auth('farmer')->user()->allowance, ' đ')}}
                            </a>
                            <div class="dropdown-divider mb-0"></div>
                            <a class="dropdown-item" href="/nong-dan/dang-xuat.html">
                                <i class="bx bx-power-off mr-50"></i> Đăng xuất
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{route('farmers.homepage')}}">
                    <div class="brand-logo">
                        <svg class="logo" width="26px" height="26px" viewbox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <title>NSP</title>
                            <defs>
                                <lineargradient id="linearGradient-1" x1="50%" y1="0%" x2="50%" y2="100%">
                                    <stop stop-color="#5A8DEE" offset="0%"></stop>
                                    <stop stop-color="#699AF9" offset="100%"></stop>
                                </lineargradient>
                                <lineargradient id="linearGradient-2" x1="0%" y1="0%" x2="100%" y2="100%">
                                    <stop stop-color="#FDAC41" offset="0%"></stop>
                                    <stop stop-color="#E38100" offset="100%"></stop>
                                </lineargradient>
                            </defs>
                            <g id="Sprite" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="sprite" transform="translate(-69.000000, -61.000000)">
                                    <g id="Group" transform="translate(17.000000, 15.000000)">
                                        <g id="icon" transform="translate(52.000000, 46.000000)">
                                            <path id="Combined-Shape" d="M13.5909091,1.77272727 C20.4442608,1.77272727 26,7.19618701 26,13.8863636 C26,20.5765403 20.4442608,26 13.5909091,26 C6.73755742,26 1.18181818,20.5765403 1.18181818,13.8863636 C1.18181818,13.540626 1.19665566,13.1982714 1.22574292,12.8598734 L6.30410592,12.859962 C6.25499466,13.1951893 6.22958398,13.5378796 6.22958398,13.8863636 C6.22958398,17.8551125 9.52536149,21.0724191 13.5909091,21.0724191 C17.6564567,21.0724191 20.9522342,17.8551125 20.9522342,13.8863636 C20.9522342,9.91761479 17.6564567,6.70030817 13.5909091,6.70030817 C13.2336969,6.70030817 12.8824272,6.72514561 12.5388136,6.77314791 L12.5392575,1.81561642 C12.8859498,1.78721495 13.2366963,1.77272727 13.5909091,1.77272727 Z"></path>
                                            <path id="Combined-Shape" d="M13.8863636,4.72727273 C18.9447899,4.72727273 23.0454545,8.82793741 23.0454545,13.8863636 C23.0454545,18.9447899 18.9447899,23.0454545 13.8863636,23.0454545 C8.82793741,23.0454545 4.72727273,18.9447899 4.72727273,13.8863636 C4.72727273,13.5378966 4.74673291,13.1939746 4.7846258,12.8556254 L8.55057141,12.8560055 C8.48653249,13.1896162 8.45300462,13.5340745 8.45300462,13.8863636 C8.45300462,16.887125 10.8856023,19.3197227 13.8863636,19.3197227 C16.887125,19.3197227 19.3197227,16.887125 19.3197227,13.8863636 C19.3197227,10.8856023 16.887125,8.45300462 13.8863636,8.45300462 C13.529522,8.45300462 13.180715,8.48740462 12.8430777,8.55306931 L12.8426531,4.78608796 C13.1851829,4.7472336 13.5334422,4.72727273 13.8863636,4.72727273 Z" fill="#4880EA"></path>
                                            <path id="Combined-Shape" d="M13.5909091,1.77272727 C20.4442608,1.77272727 26,7.19618701 26,13.8863636 C26,20.5765403 20.4442608,26 13.5909091,26 C6.73755742,26 1.18181818,20.5765403 1.18181818,13.8863636 C1.18181818,13.540626 1.19665566,13.1982714 1.22574292,12.8598734 L6.30410592,12.859962 C6.25499466,13.1951893 6.22958398,13.5378796 6.22958398,13.8863636 C6.22958398,17.8551125 9.52536149,21.0724191 13.5909091,21.0724191 C17.6564567,21.0724191 20.9522342,17.8551125 20.9522342,13.8863636 C20.9522342,9.91761479 17.6564567,6.70030817 13.5909091,6.70030817 C13.2336969,6.70030817 12.8824272,6.72514561 12.5388136,6.77314791 L12.5392575,1.81561642 C12.8859498,1.78721495 13.2366963,1.77272727 13.5909091,1.77272727 Z" fill="url(#linearGradient-1)"></path>
                                            <rect id="Rectangle" x="0" y="0" width="7.68181818" height="7.68181818"></rect>
                                            <rect id="Rectangle" fill="url(#linearGradient-2)" x="0" y="0" width="7.68181818" height="7.68181818"></rect>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <h2 class="brand-text mb-0">Utlong</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="bx-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
{{--    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="">
            <li class="nav-item">
                <a href="/nong-dan/danh-sach-lich.html">
                    <i class="bx bxs-news"></i>
                    <span class="menu-title text-truncate" data-i18n="Lịch đã đặt">Lịch đã đặt</span>
                </a>
            </li>
        </ul>
    </div>--}}
</div>
{{--end menu--}}
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper" style="padding-left: 7px; padding-right: 7px">
        <div class="content-body">
            @include('be.layouts.blocks.notify')
            @yield('content')
        </div>
    </div>
</div>
<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-left d-inline-block">2022 &copy; Utlong</span><span class="float-right d-sm-inline-block d-none">Crafted with<i class="bx bxs-heart pink mx-50 font-small-3"></i>by<a class="text-uppercase" href="https://nsp.com.vn/" target="_blank">Utlong</a></span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
    </p>
</footer>
<!-- END: Footer-->
@include('be.layouts.blocks.javascript')
</body>
<!-- END: Body-->
</html>
