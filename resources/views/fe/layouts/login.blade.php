<!doctype html>
<html lang="en">
    <head>
        <title>Đăng nhập</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{asset('public/fe/login/css/style.css?refresh=1')}}">

    </head>
    <body class="img js-fullheight" style="background-image: url({{asset('public/fe/login/images/bg.jpg')}}); background-color: #04AA6D; ">
    <section class="ftco-section">
        <div class="logo">
            <div class="row justify-content-center">
                <a href="/"><img style="  display: block;margin-left: auto;margin-right: auto;" src="/public/fe/images/logo/logo.png" alt="logo" width="45%"></a>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-4">
                    <div class="login-wrap p-0">
                        <h3 class="mb-4 text-center" style="font-weight: 700; padding-top: 50px;">ĐĂNG NHẬP</h3>
                        <form action="{{asset($action)}}" class="signin-form" method="post">
                                @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone_number" placeholder="Số điện thoại" required>
                            </div>

                            <div class="form-group">
                                <input id="password-field" type="password" class="form-control" name="password" placeholder="Mật khẩu" required>
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="form-control btn btn-primary submit px-3">ĐĂNG NHẬP</button>
                            </div>
                            <div class="form-group d-md-flex">
                                <div class="w-50">
                                    <label class="checkbox-wrap checkbox-primary">Ghi nhớ đăng nhập
                                        <input type="checkbox" checked name="remember-me">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="w-50 text-md-right">
{{--                                    <a href="#" style="color: #fff">Forgot Password</a>--}}
                                </div>
                            </div>
                        </form>

                        <div class="social d-flex text-center">
                            <a class="register" href="{{asset($register)}}"><button class="form-control btn btn-register">Bạn là người mới? Đăng kí ngay</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="{{asset('public/fe/login/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/fe/login/js/popper.js')}}"></script>
    <script src="{{asset('public/fe/login/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/fe/login/js/main.js')}}"></script>

    </body>
</html>

