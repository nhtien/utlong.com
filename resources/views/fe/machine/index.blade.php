@extends('fe.layouts.machine-main')
@section('content')
    <section>
        <div class="card-body" style="background-color: #fff; margin-bottom: 10px; padding-right: 7px; padding-left: 7px; padding-bottom: 0px">
            <ul class="nav nav-tabs" role="tablist" style="border-bottom: 0px">
                <li class="nav-item" style="margin-right: 10px;">
                    <a class="nav-link @if($status == 'unfinished') active @endif" href="/chu-may/danh-sach-lich.html?status=unfinished">
                        <i class="bx bx-x align-middle"></i>
                        <span class="align-middle">Chưa thực hiện</span>
                    </a>
                </li>
                <li class="nav-item" style="margin-right: 0px">
                    <a class="nav-link @if($status == 'complete' && $paymentStatus == null) active @endif" href="/chu-may/danh-sach-lich.html?status=complete">
                        <i class="bx bx-check align-middle"></i>
                        <span class="align-middle">Đã thực hiện</span>
                    </a>
                </li>
                <li class="nav-item" style="margin-right: 0px">
                    <a class="nav-link @if($paymentStatus == 'unpaid' && $status == 'complete') active @endif" href="/chu-may/danh-sach-lich.html?payment-status=unpaid&status=complete">
                        <i class="bx bx-x align-middle"></i>
                        <span class="align-middle">Chưa thanh toán</span>
                    </a>
                </li>
                <li class="nav-item" style="margin-right: 0px">
                    <x-BE.Action title="">
                        <a type="button" class="btn btn-outline-primary mr-1 mb-1" href="/chu-may/dat-lich.html">
                            <i class="bx bxs-add-to-queue"></i>
                            Tạo đơn
                        </a>
                    </x-BE.Action>
                </li>
            </ul>
        </div>
    </section>
    <section id="clone-lists">
        <div class="row">
            @foreach($orders as $order)
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                @if($order->order_type == 1)
                                <h5 class="my-1"><strong style="color: #39DA8A;">{{$order->farmer_full_name}}</strong> - <span style="font-size: 14px;"><a
                                            href="tel:+{{$order->farmer_phone_number}}">{{$order->farmer_phone_number}}</a></span></h5>
                                <h6>{{$order->service_name}}: {{$order->appointment_date}} (<strong>{{date_for_humans($order->getRawOriginal('appointment_date'), now())}}</strong>)</h6>
                                <h6>Địa chỉ: {{$order->farmer_address}}, {{$order->farmer_ward}}</h6>
                                @else
                                    <h5 class="my-1"><strong style="color: #39DA8A;">{{$order->cus_name}}</strong> @if($order->cus_phone != "") - <span style="font-size: 14px;"><a
                                                href="tel:+{{$order->cus_phone}}">{{$order->cus_phone}}</a></span>@endif</h5>
                                    <h6>{{$order->service_name}}: {{$order->appointment_date}} (<strong>{{date_for_humans($order->getRawOriginal('appointment_date'), now())}}</strong>)</h6>
                                    <h6>Địa chỉ: {{$order->cus_address}}</h6>
                                @endif
                                <div>
                                    <h6 style="display: inline-block; width: 85%">Số công: {{$order->plot_of_land}} công. @if($order->plot_of_bottle > 0)Phun {{$order->plot_of_bottle}} bình @endif</h6>
                                    @if($order->status === 'unfinished' || $order->payment_status === 'unpaid')
                                    <div style="display: inline-block; width: 10%" class="fonticon-wrap">
                                        <a href="{{route('machine.show', ['id' => $order->ord_id])}}"><i class="bx bx-link-external"></i></a>
                                    </div>
                                    @endif
                                </div>
{{--                                <h6 style="display: inline-block; width: 85%">Đơn giá: {{$order->area_name}}</h6>--}}
                                @if($order->allowance > 0)
                                    <h6>UtLong trả: {{currency_format($order->allowance, ' đ')}}</h6>
                                @endif
                                <h6>Tiền thu: {{currency_format($order->wallet, ' đ')}}</h6>
                                @if($order->noted)
                                    <h6>Ghi chú: "{{$order->noted}}"</h6>
                                @endif
                                <div id="chips-list-1">
                                    @if($order->status == 'complete')
                                        <div class="chip chip-success draggable mr-1">
                                            <div class="chip-body">
                                                <span class="chip-text">Đã thực hiện</span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="chip draggable mr-1">
                                            <div class="chip-body">
                                                <span class="chip-text">Chưa thực hiện</span>
                                            </div>
                                        </div>
                                    @endif
                                    @if($order->payment_status == 'paid')
                                        <div class="chip chip-success draggable mr-1">
                                            <div class="chip-body">
                                                <span class="chip-text">Đã thanh toán</span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="chip draggable mr-1">
                                            <div class="chip-body">
                                                <span class="chip-text">Chưa thanh toán</span>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </section>
    {{ $orders->links('be.blocks.pagination') }}
@endsection
