@extends('fe.layouts.machine-main')
@section('content')
    <x-BE.Action title="Đặt lịch [Sửa]">
        <x-BE.Elements.Button type="go-back" href="/chu-may/danh-sach-lich.html"></x-BE.Elements.Button>
    </x-BE.Action>
    <section id="clone-lists">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                @if($order->order_type == 1)
                                    <h5 class="my-1"><strong style="color: #39DA8A;">{{$order->farmer_full_name}}</strong> - <span style="font-size: 14px;"><a
                                                href="tel:+{{$order->farmer_phone_number}}">{{$order->farmer_phone_number}}</a></span></h5>
                                    <h6>{{$order->service_name}}: {{$order->appointment_date}} (<strong>{{date_for_humans($order->getRawOriginal('appointment_date'), now())}}</strong>)</h6>
                                    <h6>Địa chỉ: {{$order->farmer_address}}, {{$order->farmer_ward}}</h6>
                                @else
                                    <h5 class="my-1"><strong style="color: #39DA8A;">{{$order->cus_name}}</strong> @if($order->cus_phone != "") - <span style="font-size: 14px;"><a
                                                href="tel:+{{$order->cus_phone}}">{{$order->cus_phone}}</a></span>@endif</h5>
                                    <h6>{{$order->service_name}}: {{$order->appointment_date}} (<strong>{{date_for_humans($order->getRawOriginal('appointment_date'), now())}}</strong>)</h6>
                                    <h6>Địa chỉ: {{$order->cus_address}}</h6>
                                @endif
                                <div id="chips-list-1">
                                    @if($order->status == 'complete')
                                        <div class="chip chip-success draggable mr-1">
                                            <div class="chip-body">
                                                <span class="chip-text">Đã thực hiện</span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="chip draggable mr-1">
                                            <div class="chip-body">
                                                <span class="chip-text">Chưa thực hiện</span>
                                            </div>
                                        </div>
                                    @endif
                                    @if($order->payment_status == 'paid')
                                        <div class="chip chip-success draggable mr-1">
                                            <div class="chip-body">
                                                <span class="chip-text">Đã thanh toán</span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="chip draggable mr-1">
                                            <div class="chip-body">
                                                <span class="chip-text">Chưa thanh toán</span>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="card-body" style="background-color: #fff">
            <ul class="nav nav-tabs" role="tablist" style="border: 0px; margin-bottom: 0px">
                @if($order->status === 'unfinished')
                    <li class="nav-item" style="width: 100%;text-align: center;">
                        <a class="nav-link active" href="#" data-toggle="modal" data-target="#confirm-status">
                            <i class="bx bx-check align-middle"></i>
                            <span class="align-middle" style="text-transform: uppercase">Xác nhận đã thực hiện</span>
                        </a>
                        <div class="modal fade text-left" id="confirm-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel160">Xác nhận</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Chọn '<strong>Có</strong>' nếu xác nhận rằng <strong style="text-transform: uppercase">Đã thực hiện</strong>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-secondary js-confirm-status__cancel" data-dismiss="modal">
                                   {{--         <i class="bx bx-x d-block --}}{{--d-sm-none--}}{{--"></i>--}}
                                            <span class="{{--d-none--}} d-sm-block">Hủy</span>
                                        </button>
                                        <button type="button" class="btn btn-primary ml-1 js-confirm-status__ok" data-dismiss="modal">
                                            {{--<i class="bx bx-check d-block --}}{{--d-sm-none--}}{{--"></i>--}}
                                            <span class="{{--d-none--}} d-sm-block">Có</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endif
                @if($order->payment_status === 'unpaid')
                    <li class="nav-item" style="width: 100%;text-align: center;">
                        <a class="nav-link active" href="{{route('machine.confirm-payment-status', ['id' => $order->ord_id])}}" data-toggle="modal" data-target="#confirm-payment-status">
                            <i class="bx bx-check align-middle"></i>
                            <span class="align-middle" style="text-transform: uppercase">Xác nhận đã thanh toán</span>
                        </a>
                        <div class="modal fade text-left" id="confirm-payment-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1601" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel1601">Xác nhận</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i class="bx bx-x"></i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Chọn '<strong>Có</strong>' nếu xác nhận rằng <strong style="text-transform: uppercase">Đã thanh toán</strong>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-secondary js-confirm-payment-status__cancel" data-dismiss="modal">
                                       {{--     <i class="bx bx-x d-block --}}{{--d-sm-none--}}{{--"></i>--}}
                                            <span class="{{--d-none--}} d-sm-block">Hủy</span>
                                        </button>
                                        <button type="button" class="btn btn-primary ml-1 js-confirm-payment-status__ok" data-dismiss="modal">
                                           {{-- <i class="bx bx-check d-block --}}{{--d-sm-none--}}{{--"></i>--}}
                                            <span class="{{--d-none--}} d-sm-block">Có</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </section>
@endsection
@section('javascript_tag')
    @parent
    <script>
        $('.js-confirm-status__ok').click(function () {
            window.location.href= "<?php echo route('machine.confirm-status', ['id' => $order->ord_id])?>";
        });

        $('.js-confirm-payment-status__ok').click(function () {
            window.location.href= "<?php echo route('machine.confirm-payment-status', ['id' => $order->ord_id])?>";
        });
    </script>
@endsection
