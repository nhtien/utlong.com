@extends('fe.layouts.register')
@section('content')
    <h1 class="title" style="text-transform: uppercase; text-align: center; font-size: 40px;">Chủ máy</h1>
    <h2 class="title">Đăng ký Tài khoản</h2>
    @if ($errors->any())
        <div style="border-radius: 10px; border: 1px solid #f5c6cb; background-color: #f8d7da; margin-bottom: 20px; padding: 15px; color: #721c24">
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    <form method="POST" action="{{asset('/chu-may/dang-ky.html')}}">
        @csrf
        <div class="input-group">
            <label class="label">Tên máy</label>
            <input class="input--style-4" type="text" name="machine_name" placeholder="" value="{{old('machine_name')}}">
        </div>
        <div class="row row-space">
            <div class="col-2">
                <div class="input-group">
                    <label class="label">Họ và Tên</label>
                    <input class="input--style-4" type="text" name="full_name" value="{{old('full_name')}}">
                </div>
            </div>
            <div class="col-2">
                <div class="input-group">
                    <label class="label">Số điện thoại</label>
                    <input class="input--style-4" type="number" name="phone_number" placeholder="" value="{{old('phone_number')}}">
                </div>
            </div>
        </div>
        <div class="input-group">
            <label class="label">Mật khẩu</label>
            <input class="input--style-4" type="password" name="password" placeholder="Mật khẩu ít nhất 6 ký tự">
        </div>
        <div class="input-group">
            <label class="label">Tỉnh / Thành phố</label>
            <div class="rs-select2 js-select-simple select--no-search">
                <select name="province" class="js-choose-province">
                    <option disabled="disabled" selected="selected">-- Chọn Tỉnh / Thành Phố --</option>
                    @foreach( $provinces as $province)
                        <option value="{{$province->id}}">{{$province->name}}</option>
                    @endforeach
                </select>
                <div class="select-dropdown"></div>
            </div>
        </div>
        <div class="row row-space" style="margin-top: 15px">
            <div class="col-2">
                <label class="label">Quận / Huyện</label>
                <div class="rs-select2 js-select-simple select--no-search">
                    <select name="district" class="js-choose-district">
                        {{--<option disabled="disabled" selected="selected">-- Chọn Quận / Huyện --</option>--}}
                    </select>
                    <div class="select-dropdown"></div>
                </div>
            </div>
            <div class="col-2">
                <label class="label">Phường / Xã</label>
                <div class="rs-select2 js-select-simple select--no-search">
                    <select name="ward" class="js-choose-ward">
                        {{--<option disabled="disabled" selected="selected">-- Chọn Phường / Xã --</option>--}}
                    </select>
                    <div class="select-dropdown"></div>
                </div>
            </div>
        </div>
        <div class="input-group" style="margin-top: 15px">
            <label class="label">Địa chỉ</label>
            <input class="input--style-4" type="text" name="address_detail" placeholder="Số nhà, tên đường" value="{{old('address_detail')}}">
        </div>
        <div class="p-t-15">
            <button class="btn btn--radius-2 btn--blue" type="submit" style="width: 100%">Xác nhận</button>
        </div>
    </form>
@endsection
