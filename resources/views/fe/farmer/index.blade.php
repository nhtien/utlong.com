@extends('fe.layouts.farmer-main')
@section('content')
    <x-BE.Action title="Danh sách Lịch đã đặt ">
        <a type="button" class="btn btn-outline-primary mr-1 mb-1" href="/nong-dan/dat-lich.html">
            <i class="bx bxs-add-to-queue"></i>
            Đặt lịch
        </a>
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>Loại dịch vụ</th>
            <th>Loại đất</th>
            <th>Ngày thực hiện</th>
            <th>Ghi chú</th>
            <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$order->ord_id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$order->service_name}}</td>
                <td>{{$order->area_name}}</td>
                <td>{{$order->appointment_date}}</td>
                <td>{{$order->noted}}</td>
                <td>
                    <span class="{{($order->status == 'complete') ? 'text-info' : 'text-danger'}}">{{$ordStatus[$order->status]}}</span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $orders->links('be.blocks.pagination') }}
@endsection
