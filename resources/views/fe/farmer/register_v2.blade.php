<!doctype html>
<html lang="en">
<head>
    <title>Đăng nhập</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('public/fe/login/css/style.css?refresh=1')}}">
    <style>

        /* Mark input boxes that gets an error on validation: */
        input.invalid {
            background-color: #f8d7da;
        }

        /* Hide all steps by default: */
        .tab {
            display: none;
        }

        #prevBtn {
            background-color: #bbbbbb;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbbbbb;
            border: none;
            border-radius: 50%;
            display: inline-block;
            opacity: 0.5;
        }

        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #04AA6D;
        }
    </style>
</head>
<body class="img js-fullheight" style="background-image: url({{asset('public/fe/login/images/bg.jpg')}}); background-color: #04AA6D;">
<section class="ftco-section">
    <div class="logo">
        <div class="row justify-content-center">
            <a href="/"><img style="  display: block;margin-left: auto;margin-right: auto;" src="/public/fe/images/logo/logo.png" alt="logo" width="45%"></a>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="login-wrap p-0">
                    <h3 class="mb-4 text-center" style="font-weight: 700; padding-top: 50px;">ĐĂNG KÍ TÀI KHOẢN</h3>
                    <form id="regForm" class="signin-form" action="{{asset('/nong-dan/dang-ky.html')}}" method="post">
                        @if ($errors->any())
                            <div class="form-error-message">
                                @foreach ($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        @csrf
                        <div class="tab">
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone_number" placeholder="Số điện thoại" value="{{old('phone_number')}}">
                            </div>
                            <div class="form-group">
                                <input id="password-field" placeholder="Nhập mật khẩu" class="form-control" name="password" type="password"></p>
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>
                            <div class="form-group">
                                <input id="repassword-field" placeholder="Xác nhận mật khẩu" class="form-control" name="password_confirmation" type="password">
                                <span toggle="#repassword-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>
                        </div>
                        <div class="tab">
                            <div class="form-group">
                                <input class="form-control" placeholder="Diện tích (số công)" type="number" min="5" name="area" value="{{old('area')}}">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Họ và tên" type="text" name="full_name" value="{{old('full_name')}}">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Tỉnh An Giang" type="hidden" name="province" value="57">
                                {{--<select name="province" class="form-control">--}}
                                {{--<option value="57">Tỉnh An Giang</option>--}}
                                {{--</select>--}}
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Huyện Phú Tân" type="hidden" name="district" value="637">
                                {{--<select name="province" class="form-control">--}}
                                {{--<option value="57">Tỉnh An Giang</option>--}}
                                {{--</select>--}}
                            </div>
                            <div class="form-group">
                                <select name="ward" class="form-control">
                                    <option value="">-- Chọn Xã --</option>
                                    @foreach($wards as $ward)
                                        <option value="{{$ward->id}}" @selected($ward->id == old('ward'))>{{$ward->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="address_detail" placeholder="Số nhà, tên đường" >{{old('address_detail')}}</textarea>
                            </div>

                            <div class="form-group">
                                <input class="form-control" name="referral_code" placeholder="Mã giới thiệu" >{{old('referral_code')}}</input>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="button" id="nextBtn" class="form-control btn btn-primary submit px-3" onclick="nextPrev(1)" >Tiếp tục</button>
                            <button type="button" id="prevBtn" class="form-control btn btn-primary submit px-3" onclick="nextPrev(-1)" style="width: 50%">Quay lại</button>
                        </div>
                        <div style="text-align:center;margin-top:40px;">
                            <span class="step"></span>
                            <span class="step"></span>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{asset('public/fe/login/js/jquery.min.js')}}"></script>
<script src="{{asset('public/fe/login/js/popper.js')}}"></script>
<script src="{{asset('public/fe/login/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/fe/login/js/main.js')}}"></script>

<script>
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";

        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Gửi";
            document.getElementById("nextBtn").style.width = "50%";
            document.getElementById("nextBtn").style.float = "right";

        } else {
            document.getElementById("nextBtn").innerHTML = "Tiếp tục";
            document.getElementById("nextBtn").style.width = "100%";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        $(".form-error-message").hide();
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;

        if (currentTab < (x.length - 1)) {
            x[currentTab].style.display = "none";
        } else if (n == -1){
            x[currentTab].style.display = "none";
        }

        currentTab = currentTab + n;
        if (currentTab >= x.length) {
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
</script>

</body>
</html>

