@extends('fe.layouts.farmer-main')@section('content')
    <x-BE.Action title="LỊCH ĐÃ ĐẶT">
        <a type="button" class="btn btn-outline-primary mr-1 mb-1" href="/nong-dan/dat-lich.html">
            <i class="bx bxs-add-to-queue"></i>
            Đặt lịch
        </a>
    </x-BE.Action>
    <section>
        <div class="card-body" style="background-color: #fff; margin-bottom: 10px; padding-right: 7px; padding-left: 7px; padding-bottom: 0px">
            <ul class="nav nav-tabs" role="tablist" style="border-bottom: 0px">
                <li class="nav-item" style="margin-right: 10px;">
                    <a class="nav-link @if($status == 'unfinished') active @endif" href="/nong-dan/danh-sach-lich.html">
                        <i class="bx bx-x align-middle"></i>
                        <span class="align-middle">Chưa thực hiện</span>
                    </a>
                </li>
                <li class="nav-item" style="margin-right: 0px">
                    <a class="nav-link @if($status == 'complete') active @endif" href="/nong-dan/danh-sach-lich.html?status=complete">
                        <i class="bx bx-check align-middle"></i>
                        <span class="align-middle">Đã thực hiện</span>
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <section id="clone-lists">
        <div class="row">
            @foreach($orders as $order)
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <h5><strong style="color: #39DA8A;">{{$order->service_name}}:</strong> <span style="font-size: 14px;">{{$order->appointment_date}}</span></h5>
                                <h6>Diện tích: {{$order->plot_of_land}} công. @if($order->plot_of_bottle > 0) Phun {{$order->plot_of_bottle}} bình @endif </h6>
                                @if($order->status == 'unfinished')
                                    <h6 style="display: inline-block; width: 85%">Đơn giá: {{$order->area_name}}</h6>
                                @else
                                    <h6>{{$order->area_name}}</h6>
                                @endif
                                @if($order->allowance > 0)
                                    <h6>Khuyến mãi: {{currency_format($order->allowance, ' đ')}}</h6>
                                @endif

                                <h6>Tổng tiền: {{currency_format($order->wallet, ' đ')}}</h6>
                                <div id="chips-list-1">
                                    @if($order->status == 'complete')
                                        <div class="chip chip-success draggable mr-1">
                                            <div class="chip-body">
                                                <span class="chip-text">Đã thực hiện</span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="chip draggable mr-1">
                                            <div class="chip-body">
                                                <span class="chip-text">Chưa thực hiện</span>
                                            </div>
                                        </div>

                                        <a href="{{route('farmer.edit', ['id' => $order->ord_id])}}">
                                            <div class="chip draggable chip-success mr-1">
                                                <div class="chip-body">
                                                    <span class="chip-text">Sửa lịch phun</span>
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </section>
    {{ $orders->links('be.blocks.pagination') }}
@endsection
