@extends('fe.layouts.farmer-main')
@section('content')
    <x-BE.Action title="Đặt lịch [Sửa]">
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="/nong-dan/cap-nhat-lich-dat.html">
            <input type="hidden" name="ord_id" value="{{$order->ord_id}}">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="form-group">
                        <label class="d-block">Loại dịch vụ</label>
                        <div class="custom-control-inline">
                            @foreach($services as $key => $item)
                                @php
                                    $checked = old('service_id') === null ? ((int)$order->service_id === (int)$item->service_id) : ((int)old('service_id') === (int)$item->service_id);
                                @endphp
                            <div class="radio mr-1">
                                <input type="radio" name="service_id" value="{{$item->service_id}}" id="service_radio_{{$key}}" @checked($checked)>
                                <label style="font-size: 12px" for="service_radio_{{$key}}">{{$item->name}}</label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <div class="form-group">
                        <div class="controls">
                            <label for="appointment_date">Ngày thực hiện</label>
                            <input value="{{old('appointment_date') ?? $order->appointment_date}}"  type="text" placeholder="Ngày thực hiện"  name="appointment_date" id="appointment_date" class="form-control js-customize-datepicker">
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12">
                    <div class="form-group">
                        <label class="d-block">Loại giá</label>
                        <div class="custom-control-inline">
                            @foreach($areas as $key => $item)
                                @php
                                    $checked = old('area_id') === null ? ((int)$item->area_id === (int)$order->area_id) : ((int)old('area_id') === (int)$item->area_id);
                                @endphp
                                <div class="radio mr-1">
                                    <input type="radio" data-fee="{{$item->fee}}" name="area_id" value="{{$item->area_id}}" id="area_radio_{{$key}}" @checked($checked)>
                                    <label style="font-size: 12px" for="area_radio_{{$key}}">{{$item->name}}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <div class="form-group">
                        <h6 class="d-block"><span style="color:red; font-style: italic;"> 18.000đ/công đảm bảo phun 2,5 lít/công</span></h6>
                        <h6 class="d-block"><span style="color:red; font-style: italic;">120.000đ/bình đảm bảo phun 25 lít/bình</span></h6>
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <div class="form-group">
                        <div class="controls">
                            <label for="plot_of_land">Diện tích (số công)</label>
                            <input value="{{old('plot_of_land') ?? $order->plot_of_land}}"  type="number" placeholder="Nhập số công" min="5"  name="plot_of_land" id="plot_of_land" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12" @if($order->plot_of_bottle <= 0) style="display: none;" @endif>
                    <div class="form-group">
                        <div class="controls">
                            <label for="plot_of_bottle">Số bình muốn phun</label>
                            <input value="{{old('plot_of_bottle') ?? $order->plot_of_bottle}}"  type="number" placeholder="Nhập số công" min="5"  name="plot_of_bottle" id="plot_of_bottle" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <div class="form-group">
                        <div class="controls">
                            <label for="noted">Thông tin thêm</label>
                            <textarea class="form-control" placeholder=""  id="noted" name="noted" rows="3" >{{old('noted') ?? $order->noted}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item d-flex justify-content-between border-0 pb-0">
                        <span class="invoice-subtotal-title">Đơn giá:</span>
                        <h6 class="invoice-subtotal-value mb-0 js-fee"></h6>
                    </li>
                    <li class="list-group-item d-flex justify-content-between border-0 pb-0">
                        <span class="invoice-subtotal-title">Số công:</span>
                        <h6 class="invoice-subtotal-value mb-0 js-plot-of-land"></h6>
                    </li>
                    <li class="list-group-item d-flex justify-content-between border-0 pb-0 plot-of-bottle-hide">
                        <span class="invoice-subtotal-title">Số bình:</span>
                        <h6 class="invoice-subtotal-value mb-0 js-plot-of-bottle"></h6>
                    </li>
{{--                    <li class="list-group-item d-flex justify-content-between border-0 pb-0">--}}
{{--                        <span class="invoice-subtotal-title">Phí dịch vụ:</span>--}}
{{--                        <h6 class="invoice-subtotal-value mb-0 js-service-fee"></h6>--}}
{{--                    </li>--}}
                    <li class="list-group-item d-flex justify-content-between border-0 pb-0">
                        <span class="invoice-subtotal-title">Tiền khuyến mãi:</span>
                        <h6 class="invoice-subtotal-value mb-0 js-allowance"></h6>
                    </li>
                    <li class="list-group-item py-0 border-0 mt-25">
                        <hr>
                    </li>
                    <li class="list-group-item d-flex justify-content-between border-0 py-0">
                        <span class="invoice-subtotal-title"><strong>Tổng tiền:</strong></span>
                        <h6 class="invoice-subtotal-value mb-0 js-total-fee" style="font-weight: bold"></h6>
                    </li>
                </ul>
            </div>
        </div>
    </x-BE.Sections.Card>
    <x-BE.Action title="">
        <x-BE.Elements.Button type="go-back" href="/nong-dan/danh-sach-lich.html"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="save" data-url="/nong-dan/cap-nhat-lich-dat.html"></x-BE.Elements.Button>
    </x-BE.Action>
@endsection
@section('javascript_tag')
    @parent
    <script>
        function totalEstimatedCost() {
            const areaElem       = $('input[name="area_id"]:checked')[0];
            const plotOfLandElem = $('input[name="plot_of_land"]');
            const plotOfLandBottle = $('input[name="plot_of_bottle"]');
            const serviceFee     = @php echo $service_fee @endphp;
            const discountFee    = @php echo $discount_fee @endphp;

            var fee              = $(areaElem).data('fee');
            var plotOfLand       = $(plotOfLandElem).val();
            var plotOfBottle     = $(plotOfLandBottle).val();
            var totalFee = 0;
            if ($(areaElem).val() == 1) {//tính theo công 18k/công
                totalFee = (fee * plotOfLand) + serviceFee - discountFee;
                $('.js-fee').html(fee.toLocaleString() + ' đ/công');
            } else {
                $('.js-plot-of-bottle').parent().removeClass('plot-of-bottle-hide');
                $('#plot_of_bottle').parent().parent().parent().css('display', 'block');
                totalFee = (fee * plotOfBottle) + serviceFee - discountFee;
                $('.js-fee').html(fee.toLocaleString() + ' đ/bình');
            }


            $('.js-plot-of-land').html(plotOfLand.toLocaleString());
            $('.js-plot-of-bottle').html(plotOfBottle.toLocaleString());
            $('.js-service-fee').html(serviceFee.toLocaleString() + ' đ');
            $('.js-allowance').html('-' + discountFee.toLocaleString() + ' đ');
            $('.js-total-fee').html(totalFee.toLocaleString() + ' đ');
        }

        totalEstimatedCost();

        $('input[name="area_id"]').change(function () {
            let areaElem       = $('input[name="area_id"]:checked')[0];
            if ($(areaElem).val() == 2) {
                $('.js-plot-of-bottle').parent().removeClass('plot-of-bottle-hide');
                $('#plot_of_bottle').parent().parent().parent().css('display', 'block');
                var plot = $('input[name="plot_of_land"]').val();
                var bottle = Math.ceil(plot / 10);
                $('input[name="plot_of_bottle"]').val(bottle);
            } else {
                $('.js-plot-of-bottle').parent().addClass('plot-of-bottle-hide');
                $('#plot_of_bottle').parent().parent().parent().css('display', 'none');
            }
            totalEstimatedCost();
        });

        $('input[name="plot_of_land"]').change(function() {
            var plot = $('input[name="plot_of_land"]').val();
            var bottle = Math.ceil(plot / 10);
            $('input[name="plot_of_bottle"]').val(bottle);
            totalEstimatedCost();
        });

        $('input[name="plot_of_bottle"]').change(function() {
            totalEstimatedCost();
        });

    </script>
@endsection
