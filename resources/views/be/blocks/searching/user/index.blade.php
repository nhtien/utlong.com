@php
    $filters = request()->get('filter', []);
@endphp
<x-BE.Searching title="Tìm kiếm" form-action="{{route('be.users.index')}}">
    <x-BE.Searching.SearchBox placeholder="Họ và Tên, email, tài khoản"></x-BE.Searching.SearchBox>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[usr_group_id]">Nhóm</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[usr_group_id]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($userGroups as $key => $userGroup)
                    <option value="{{$userGroup->usrgroup_id}}" @selected($userGroup->usrgroup_id == @$filters['usr_group_id'])>{{$userGroup->name}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[usr_status]">Trạng thái</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[usr_status]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($usrStatus as $key => $item)
                    <option value="{{$key}}" @selected($key == @$filters["usr_status"]) >{{$item}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <x-BE.Searching.ActionBox refresh-url="{{route('be.users.index')}}"></x-BE.Searching.ActionBox>
</x-BE.Searching>
