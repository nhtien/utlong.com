@php
    $filters = request()->get('filter', []);
@endphp
<x-BE.Searching title="Tìm kiếm" form-action="{{route('be.post.index')}}">
    <x-BE.Searching.SearchBox placeholder="Tiêu đề, nội dung"></x-BE.Searching.SearchBox>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[pst_grp_id]">Nhóm Bài Viết</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[pst_grp_id]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($postGroups as $key => $postGroup)
                    <option value="{{$postGroup->pgrp_id}}" @selected($postGroup->pgrp_id == @$filters['pst_grp_id'])>{{$postGroup->name}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[pst_status]">Trạng thái</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[pst_status]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($pstStatus as $key => $item)
                    <option value="{{$key}}" @selected($key == @$filters["pst_status"]) >{{$item}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <x-BE.Searching.ActionBox refresh-url="{{route('be.post.index')}}"></x-BE.Searching.ActionBox>
</x-BE.Searching>
