@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Thông tin cá nhân [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.users.update-profile')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('dashboard-backend')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.users.update')}}">
            @method('PUT')
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="usr_username" value="{{old('usr_username') ?? $user->username}}" disabled/>
                    <x-BE.Elements.Input id="usr_full_name" value="{{old('usr_full_name') ?? $user->name}}"/>
                    <x-BE.Elements.Input id="usr_email" value="{{old('usr_email') ?? $user->email}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="Nhóm" value="{{optional($user->userGroup)->name}}" disabled/>
                    <x-BE.Elements.Input type="password" id="usr_password" value="">
                        <x-slot name="warning">Những ký tự được phép là chữ, số và !@#$%^&*</x-slot>
                    </x-BE.Elements.Input>
                    <x-BE.Elements.Input type="password" id="password_confirmation" value=""/>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
