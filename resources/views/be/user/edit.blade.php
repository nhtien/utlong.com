@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Nhân viên [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.users.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.users.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.users.update')}}">
            @method('PUT')
            <input type="hidden" name="usr_id" value="{{$user->user_id}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="{{get_column_alias('users.username')}}" value="{{old(get_column_alias('users.username')) ?? $user->username}}" disabled/>
                    <x-BE.Elements.Input id="{{get_column_alias('users.name')}}"     value="{{old(get_column_alias('users.name')) ?? $user->name}}"/>
                    <x-BE.Elements.Input id="{{get_column_alias('users.email')}}"    value="{{old(get_column_alias('users.email')) ?? $user->email}}"/>
                    <div class="form-group">
                        <label for="{{get_column_alias('users.usrgroup_id')}}">Nhóm</label>
                        <select class="form-control" name="{{get_column_alias('users.usrgroup_id')}}" id="{{get_column_alias('users.usrgroup_id')}}">
                            @foreach($userGroups as $userGroup)
                                <option value="{{$userGroup->usrgroup_id}}" @selected($userGroup->usrgroup_id == $user->usrgroup_id || $userGroup->usrgroup_id == old('usr_group_id'))>{{$userGroup->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input type="password" id="{{get_column_alias('users.password')}}" value="">
                        <x-slot name="warning">Những ký tự được phép là chữ, số và !@#$%^&*</x-slot>
                    </x-BE.Elements.Input>
                    <x-BE.Elements.Input type="password" id="password_confirmation" value=""/>
                    <div class="form-group">
                        <label for="{{get_column_alias('users.status')}}">Trạng thái</label>
                        <select class="form-control" name="{{get_column_alias('users.status')}}" id="{{get_column_alias('users.status')}}">
                            @foreach($usrStatus as $key => $item)
                                <option value="{{$key}}" @selected($user->status == $key || $key == old('usr_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
