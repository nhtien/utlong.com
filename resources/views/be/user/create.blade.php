@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Nhân viên [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.users.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.users.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.users.update')}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="usr_username" value="{{old('usr_username')}}">
                        <x-slot name="warning">Những ký tự được phép là chữ và số</x-slot>
                    </x-BE.Elements.Input>
                    <x-BE.Elements.Input id="usr_full_name" value="{{old('usr_full_name')}}"/>
                    <x-BE.Elements.Input id="usr_email" value="{{old('usr_email')}}"/>
                    <div class="form-group">
                        <label for="usr_group_id">Nhóm</label>
                        <select class="form-control" name="usr_group_id" id="usr_group_id">
                            @foreach($userGroups as $userGroup)
                                <option value="{{$userGroup->usrgroup_id}}" @selected($userGroup->usrgroup_id == old('usr_group_id'))>{{$userGroup->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input type="password" id="usr_password" value="">
                        <x-slot name="warning">Những ký tự được phép là chữ, số và !@#$%^&*</x-slot>
                    </x-BE.Elements.Input>
                    <x-BE.Elements.Input type="password" id="password_confirmation" value=""/>
                    <div class="form-group">
                        <label for="usr_status">Trạng thái</label>
                        <select class="form-control" name="usr_status" id="usr_status">
                            @foreach($usrStatus as $key => $item)
                                <option value="{{$key}}" @selected( $key == old('usr_status') )>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
