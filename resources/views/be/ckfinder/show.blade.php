@php
session_start();
$_SESSION['ckf_user'] = array_merge(auth()->user()->toArray(), ['time' => time() + 60]);
@endphp
<!DOCTYPE html>
<!--
Copyright (c) 2007-2019, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or https://ckeditor.com/sales/license/ckfinder
-->
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <title>CKFinder 3 - File Browser</title>
</head>
<body>

<script src="{{asset('/public/ckfinder/ckfinder.js')}}"></script>
<script>
    var config = {};
    config.connectorPath = "/public/ckfinder/core/connector/php/connector.php";
    config.connectorInfo = "type={{$type}}&folder={{$folder}}";
    CKFinder.config(config);
    CKFinder.start();
</script>

</body>
</html>

