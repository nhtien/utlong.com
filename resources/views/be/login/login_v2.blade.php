<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Frest admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Frest admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Login Page</title>
    <link href="{{asset('/public/uploads/images/favicon/favicon_48x48.ico')}}" rel="shortcut icon">
    <link href="{{asset('/public/uploads/images/favicon/favicon_192x192.png')}}" rel="icon" sizes="192x192" type="image/png">
    <link href="{{asset('/public/uploads/images/favicon/favicon_96x96.png')}}" rel="icon" sizes="96x96" type="image/png">
    <link href="{{asset('/public/uploads/images/favicon/favicon_48x48.png')}}" rel="icon" sizes="48x48" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/pages/authentication.css">
    <!-- END: Page CSS-->


</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- login page start -->
            <section id="auth-login" class="row flexbox-container">
                <div class="col-xl-8 col-11">
                    <div class="card bg-authentication mb-0">
                        <div class="row m-0">
                            <!-- left section-login -->
                            <div class="col-md-6 col-12 px-0">
                                <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                    <div class="card-header pb-1">
                                        <div class="card-title">
                                            <h4 class="text-center mb-2">Welcome Back</h4>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        {{--<div class="d-flex flex-md-row flex-column justify-content-around">
                                            <a href="javascript:void(0);" class="btn btn-social btn-google btn-block font-small-3 mr-md-1 mb-md-0 mb-1">
                                                <i class="bx bxl-google font-medium-3"></i><span class="pl-50 d-block text-center">Google</span></a>
                                            <a href="javascript:void(0);" class="btn btn-social btn-block mt-0 btn-facebook font-small-3">
                                                <i class="bx bxl-facebook-square font-medium-3"></i><span class="pl-50 d-block text-center">Facebook</span></a>
                                        </div>--}}
                                        <div class="divider">
                                            <div class="divider-text text-uppercase text-muted"><small>login with username</small>
                                            </div>
                                        </div>
                                        <form action="/login.html" method="post">
                                            @csrf
                                            <div class="form-group mb-50">
                                                <label class="text-bold-600" for="username">Username</label>
                                                <input type="text" class="form-control" id="username" name="username" placeholder="Enter your username"></div>
                                            <div class="form-group">
                                                <label class="text-bold-600" for="password">Password</label>
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                            </div>
                                            <div class="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">
                                                <div class="text-left">
                                                    <div class="checkbox checkbox-sm">
                                                        <input type="checkbox" class="form-check-input" id="remember-me" name="remember-me">
                                                        <label class="checkboxsmall" for="remember-me"><small>Keep me logged
                                                                in</small></label>
                                                    </div>
                                                </div>
                                                {{--<div class="text-right"><a href="auth-forgot-password.html" class="card-link"><small>Forgot Password?</small></a></div>--}}
                                            </div>
                                            <button type="submit" class="btn btn-primary glow w-100 position-relative">Login<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                        </form>
                                        <hr>
                                        {{--<div class="text-center"><small class="mr-25">Don't have an account?</small><a href="auth-register.html"><small>Sign up</small></a></div>--}}
                                    </div>
                                </div>
                            </div>
                            <!-- right section image -->
                            <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                <img class="img-fluid" src="/public/be/app-assets/images/pages/login.png" alt="branding logo">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- login page ends -->

        </div>
    </div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="/public/be/app-assets/vendors/js/vendors.min.js"></script>
<script src="/public/be/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
<script src="/public/be/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
<script src="/public/be/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="/public/be/app-assets/js/scripts/configs/vertical-menu-light.js"></script>
<script src="/public/be/app-assets/js/core/app-menu.js"></script>
<script src="/public/be/app-assets/js/core/app.js"></script>
<script src="/public/be/app-assets/js/scripts/components.js"></script>
<script src="/public/be/app-assets/js/scripts/footer.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
