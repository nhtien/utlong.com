@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Nhóm nhân viên [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.usergroup.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.usergroup.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.usergroup.update')}}">
            @method('PUT')
            <input type="hidden" name="ugrp_id" value="{{$userGroup->usrgroup_id}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="ugrp_name" value="{{old('ugrp_name') ?? $userGroup->name}}"/>
                    <div class="form-group">
                        <label for="ugrp_status">Trạng thái</label>
                        <select class="form-control" name="ugrp_status" id="ugrp_status">
                            @foreach($groupStatus as $key => $item)
                                <option value="{{$key}}" @selected($userGroup->status == $key || $key == old('ugrp_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Textarea content="{{old('ugrp_des') ?? $userGroup->description}}" rows="4" id="ugrp_des"/>
                </div>
                <div class="col-12 col-sm-12">
                    <div class="form-group">
                        <label class="d-block" for="description">Quyền truy cập</label>
                        @foreach($parentMenus as $parentMenu)
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="menu-{{$parentMenu->menu_id}}" type="checkbox" class="checkbox-input" name="ugrp_menu_id[]" value="{{$parentMenu->menu_id}}" @checked(in_array($parentMenu->menu_id, $menuOfUserGroupArr))>
                                    <label for="menu-{{$parentMenu->menu_id}}">{{$parentMenu->name}}</label>
                                </div>
                            </div>
                            @if(array_key_exists($parentMenu->menu_id, $childMenuArr))
                                @foreach($childMenuArr[$parentMenu->menu_id] as $childrenMenu)
                                    <div class="form-group" style="padding-left: 35px">
                                        <div class="checkbox">
                                            <input id="menu-{{$childrenMenu['menu_id']}}" type="checkbox" class="checkbox-input" name="ugrp_menu_id[]" value="{{$childrenMenu['menu_id']}}" @checked(in_array($childrenMenu['menu_id'], $menuOfUserGroupArr))>
                                            <label for="menu-{{$childrenMenu['menu_id']}}">{{$childrenMenu['name']}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
