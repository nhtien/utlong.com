<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuFrontendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $ruleDefault = [
            'menu_fe_name'       => 'bail|required|min:1|max:255',
            'menu_fe_name_en'    => 'bail|nullable|min:1|max:255',
            'menu_fe_link'       => 'bail|nullable|min:1|max:1000',
            'menu_fe_link_en'    => 'bail|nullable|min:1|max:1000',
            'menu_fe_icon'       => 'bail|nullable|min:1|max:255',
            'menu_fe_special'    => 'bail|required|in:default,link,hot,popular,new',
            'menu_fe_status'     => 'bail|required|in:inactive,activated',
        ];
        $id         = request()->post('menu_fe_id');
        $status     = request()->post('menu_fe_status');
        $routeName  = $this->route()->getName();
        return match ($routeName) {
            'be.menufrontend.store'   => $ruleDefault,
            'be.menufrontend.update'  => array_merge($ruleDefault, [
                'menu_fe_id'        => 'bail|required|exists:menu_frontend,id',
                'menu_fe_status'    => "bail|required|in:inactive,activated|check_node_status:menu_frontend,{$id},{$status}"
            ]),
            default => [],
        };
    }
}
