<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $ruleDefault = [
            'menu_name'          => 'bail|required|min:1',
            'menu_icon'          => 'bail|required|min:2',
            'menu_controller'    => 'bail|required|min:1',
            'menu_action'        => 'bail|required|min:1',
            'menu_parent'        => 'bail|required|integer',
            'menu_position'      => 'bail|required|integer',
            'menu_status'        => 'bail|required|in:inactive,activated',
        ];
        $routeName = $this->route()->getName();
        return match ($routeName) {
            'be.menu.update'    => array_merge($ruleDefault, ['menu_id' => 'bail|required|exists:menus,menu_id']),
            'be.menu.store'     => $ruleDefault,
            default => [],
        };
    }
}
