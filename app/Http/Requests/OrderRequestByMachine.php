<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequestByMachine extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $ruleDefault = [
            'appointment_date'  => [
                'bail',
                'required',
                'date_format:d-m-Y',
                function ($attribute, $value, $fail) {
                    // Thời gian user đặt lịch < thời gian hiện tại
                    if (strtotime($value) < strtotime(date("d-m-Y"))) {
                        $fail('`Ngày thực hiện` phải sau thời gian hiện tại');
                    }

                    // Thời gian hiện tại sau 15h => bắt buộc người nhập ngày hôm sau
                    if (strtotime(date("d-m-Y H:i")) > strtotime(date("d-m-Y 15:00"))) {
                        if (strtotime($value) < strtotime(date('Y-m-d 00:00:00', strtotime("+1 day")))) {
                            $fail('`Ngày thực hiện` phải sau ngày hôm nay');
                        }
                    }
                    return true;
                }
            ],
            'service_id'        => 'bail|required|exists:services,service_id',
            'area_id'           => 'bail|required|exists:areas,area_id',
            'noted'             => 'bail|nullable|min:1|max:1000|do_not_contain_html_tag',
            'plot_of_land'      => 'bail|integer|min:5|max:10000',
            'cus_name'          => 'bail|required|min:3|max:255|do_not_contain_html_tag',
            'cus_address'       => 'bail|required|min:5|max:255|do_not_contain_html_tag',
        ];
        $routeName = $this->route()->getName();
        return match ($routeName) {
            'machine.store' => $ruleDefault,
            default => [],
        };
    }
}
