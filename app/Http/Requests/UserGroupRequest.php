<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $ruleDefault = [
            'ugrp_name'     => 'bail|required|min:3',
            'ugrp_des'      => 'bail|required|min:5',
            'ugrp_status'   => 'bail|required|in:inactive,activated',
        ];
        $routeName = $this->route()->getName();
        return match ($routeName) {
            'be.usergroup.update'=> array_merge($ruleDefault, ['ugrp_id' => 'bail|required|exists:user_groups,usrgroup_id']),
            'be.usergroup.store' => $ruleDefault,
            default          => [],
        };
    }
}
