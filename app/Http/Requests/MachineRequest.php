<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MachineRequest  extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $ruleDefault = [
            'machine_name'  => 'bail|required|between:3,255|do_not_contain_html_tag',
            'full_name'     => 'bail|required|between:4,255|do_not_contain_html_tag',
            'address_detail'=> 'bail|required|between:2,255|do_not_contain_html_tag',
            'phone_number'  => 'bail|required|phone_number|unique:machines,phone_number',
            'password'      => 'bail|required|between:6,255|password',
            'province'      => 'bail|required|exists:provinces,id',
            'district'      => 'bail|required|exists:districts,id',
            'ward'          => 'bail|required|exists:wards,id',
        ];
        $routeName = $this->route()->getName();
        return match ($routeName) {
            'machine.register'    => $ruleDefault,
            default => [],
        };
    }
}
