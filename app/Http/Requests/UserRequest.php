<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $ruleDefault = [
            'usr_username'   => 'bail|between:6,255|username|unique:users,username',
            'usr_full_name'  => 'bail|required|min:3',
            'usr_email'      => 'bail|email|nullable',
            'usr_group_id'   => 'bail|integer|required',
            'usr_status'     => 'bail|required|in:inactive,activated',
            'usr_id'         => 'bail|integer|not_in:' . implode(',', get_root_user_ids()),
            'usr_password'   => 'bail|required|same:password_confirmation|between:6,255|password',
        ];

        $routeName = $this->route()->getName();
        $rule = [];

        switch ($routeName) {
            case 'be.users.store':
                $rule = $ruleDefault;
                break;
            case 'be.users.update':
                $update = [
                    'usr_password' => 'bail|nullable|same:password_confirmation|between:6,255|password',
                    'usr_id'       => 'bail|required|exists:users,user_id'
                ];
                $rule   = array_merge($ruleDefault, $update);
                unset($rule['usr_username']);
                break;
            case 'be.users.update-profile':
                $rule = [
                    'usr_full_name'  => 'bail|required|min:3',
                    'usr_email'      => 'bail|nullable|email',
                    'usr_password'   => 'bail|nullable|same:password_confirmation|between:6,255|password',
                ];
                break;
        }

        return $rule;
    }
}
