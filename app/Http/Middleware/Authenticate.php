<?php

namespace App\Http\Middleware;

use App\Models\Farmer;
use App\Models\Menu;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use JetBrains\PhpStorm\ArrayShape;

class Authenticate extends Middleware
{
    protected string $redirectTo = 'login';

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  Request  $request
     * @return string|null
     */
    protected function redirectTo($request): ?string
    {
        if (! $request->expectsJson()) {
            return route($this->redirectTo);
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @param  string[]  ...$guards
     * @return mixed
     *
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards): mixed
    {
        $guard = $guards ? $guards[0] : $guards;
        if ($guard === 'farmer') {
            $this->redirectTo = 'farmer.login';
        } elseif ($guard === 'machine') {
            $this->redirectTo = 'machine.login';
        }

        $this->authenticate($request, $guards);

        if ($guard === 'farmer') {
            /**
             * @var $auth Farmer
             */
            $auth = Auth::guard($guard)->user();
            // logout khi status là inactive
            if ($auth->status !== 'activated') {
                return redirect()->route('farmer.logout');
            }
            //return abort(403);
        }elseif ($guard === 'machine') {
            /**
             * @var $auth Farmer
             */
            $auth = Auth::guard($guard)->user();
            // logout khi status là inactive
            if ($auth->status !== 'activated') {
                return redirect()->route('machine.logout');
            }
            //return abort(403);
        }else{
            /**
             * Kiểm tra quyền truy cập của người dùng
             */
            if ($this->checkAccess() === false) {
                // logout khi status là inactive
                if (\auth()->user()->status !== 'activated') {
                    return redirect()->route('logout');
                }
                return abort(403);
            }
        }


        return $next($request);
    }

    /**
     * @return bool
     */
    private function checkAccess(): bool
    {
        $auth = Auth::guard();
        if ($auth->check()) {
            $user = $auth->user();
            if (optional($user)->status != 'activated') {
                return false;
            } else {
                if (is_root_account() === false) {
                    if (check_access_current_action() === false) return false;
                }
                return true;
            }
        }

        return false;
    }

}
