<?php

namespace App\Http\Controllers\FE;

use App\Models\District;
use Illuminate\Http\JsonResponse;

class DistrictController extends Controller
{

    public function __construct(public District $districtModel)
    {
        parent::__construct();
    }


    /**
     * @param $province_id
     * @return JsonResponse
     */
    public function findsDistrictByProvince($province_id): JsonResponse
    {
        $districts = $this->districtModel->findsDistrictByProvince($province_id);
        return response()->json($districts);
    }
}
