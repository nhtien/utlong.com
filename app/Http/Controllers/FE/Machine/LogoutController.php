<?php

namespace App\Http\Controllers\FE\Machine;

use App\Http\Controllers\BE\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * https://laravel.com/docs/9.x/authentication#invalidating-sessions-on-other-devices
 */
class LogoutController extends Controller
{
    /**
     * @var string
     */
    private string $loginForm = '/';

    /**
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        Auth::guard('machine')->logout();
        return redirect($this->loginForm);
    }

    /**
     * form logout Other Devices
     * @return Factory|View|Application
     */
    public function logoutForm(): Factory|View|Application
    {
        return view('be.logout.form');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function logoutOtherDevices(Request $request): RedirectResponse
    {
        $password = $request->post('password', null);
        if (Hash::check($password, $request->user()->password)) {
            if (Auth::logoutOtherDevices($password)) {
                return redirect()->route('dashboard-backend')->with('success', 'Đăng xuất thành công các thiết bị khác');
            }
        }

        return back()->with('error', 'Mật khẩu không chính xác.');
    }
}
