<?php

namespace App\Http\Controllers\FE\Machine;

use App\Http\Controllers\FE\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class IndexController extends Controller
{
    public function __construct()
    {
        parent::__construct();
       // $this->mainModel = $mainModel;
    }

    public function index()
    {
        return to_route('machine.index');
    }

    /**
     * @param $id
     * @param $slug
     * @return View|Factory|RedirectResponse|Application
     */
    public function show($id, $slug): View|Factory|RedirectResponse|Application
    {
        if (!$post = $this->mainModel::parentQuery()->isActivated()->find($id)) {
            return redirect()->route('home');
        }

        return view('fe.post.show');
    }
}
