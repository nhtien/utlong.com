<?php

namespace App\Http\Controllers\FE\Machine;

use App\Http\Controllers\FE\Controller;
use App\Http\Requests\MachineRequest;
use App\Models\Machine;
use App\Models\Province;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class RegisterController extends Controller
{
    /**
     * @var string
     */
    private string $loginForm = '/chu-may/dang-nhap.html';


    public function __construct( public Province $provinceModel, public Machine $machineModel)
    {
        parent::__construct();
        $this->middleware('guest:machine');
    }


    public function register(MachineRequest $request): Redirector|RedirectResponse|Application
    {
        $this->machineModel::query()->create([
            'full_name'     => $request->post('full_name'),
            'phone_number'  => $request->post('phone_number'),
            'machine_name'  => $request->post('machine_name'),
            'status'        => 'activated',
            'address'       => $request->post('address_detail'),
            'ward_id'       => $request->post('ward'),
            'province_id'   => $request->post('province'),
            'district_id'   => $request->post('district'),
            'password'      => $request->post('password'),
        ]);
        return redirect()->route('machine.login')->with('success', 'Tạo tài khoản thành công, hãy đăng nhập !');
    }

    /**
     * @return Application|Factory|View
     */
    public function registerForm(): View|Factory|Application
    {
        $provinces = $this->provinceModel->getAll();
        return view('fe.machine.register',
            [
                'provinces' => $provinces,
            ]);
    }

}
