<?php

namespace App\Http\Controllers\FE\Machine;

use App\Http\Controllers\FE\Controller;
use App\Http\Requests\OrderRequestByMachine;
use App\Models\Area;
use App\Models\Config;
use App\Models\Order;
use App\Models\Service;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Helpers\Order as OrderHelper;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class MachineController extends Controller
{
    public function __construct(
        public Order $orderModel,
        public Service $serviceModel,
        public Area $areaModel,
        public OrderHelper $orderHelper
    )
    {
        parent::__construct();
    }

    /**
     * @return Application|Factory|View
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index()
    {
        $machineId      = auth('machine')->user()->machine_id;
        $status         = request()?->get('status', null);
        $paymentStatus  = request()?->get('payment-status', null);

        if ($status === null && $paymentStatus === null) {
            return to_route('machine.index', ['status' => 'unfinished']);
        }

        $orders = $this->orderModel::query()
            ->select([
                'orders.*',
                'services.name as service_name',
                'areas.name as area_name',
                'farmers.full_name as farmer_full_name',
                'farmers.phone_number as farmer_phone_number',
                'farmers.address as farmer_address',
                'wards.name as farmer_ward',
            ])
            ->join('services', 'services.service_id', 'orders.service_id')
            ->join('areas', 'areas.area_id', 'orders.area_id')
            ->join('farmers', 'farmers.farmer_id', 'orders.farmer_id')
            ->join('wards', 'farmers.ward_id', 'wards.id')
            ->when($status !== null, function ($query) use ($status) {
                $query->where('orders.status', $status);
            })
            ->when($paymentStatus !== null, function ($query) use ($paymentStatus) {
                $query->where('orders.payment_status', $paymentStatus);
            })
            ->where('machine_id', $machineId)
            ->orderBy('appointment_date', 'asc')
            ->paginate();
        return \view('fe.machine.index', [
            'orders'        => $orders,
            'ordStatus'     => $this->orderModel::STATUS,
            'status'        => $status,
            'paymentStatus' => $paymentStatus
        ]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show($id)
    {
        $machine = auth('machine')->user();
        $order   = $this->orderModel::query()
            ->select([
                'orders.*',
                'services.name as service_name',
                'areas.name as area_name',
                'farmers.full_name as farmer_full_name',
                'farmers.phone_number as farmer_phone_number',
            ])
            ->join('services', 'services.service_id', 'orders.service_id')
            ->join('areas', 'areas.area_id', 'orders.area_id')
            ->join('farmers', 'farmers.farmer_id', 'orders.farmer_id')
            ->where('machine_id', $machine->machine_id)
            ->find($id);
        // check auth
        $this->authorize('update-order-machine', $order);
/*        if ($order->status === 'complete' && $order->payment_status === 'paid') {
            abort(403);
        }*/

        return view('fe.machine.edit', [
            'order' => $order,
        ]);
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function confirmStatus($id)
    {
        $machine = auth('machine')->user();
        $order   = $this->orderModel::query()
            ->select([
                'orders.*',
            ])
            ->where('machine_id', $machine->machine_id)
            ->find($id);

        if ($order->status === 'complete') {
            return redirect()->route('machine.index');
        }

        $order->status = 'complete';
        $order->save();

        return redirect()->route('machine.show', ['id' => $id])->with('success', 'Xác nhận đã thực hiện thành công');
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function confirmPaymentStatus($id)
    {
        $machine = auth('machine')->user();
        $order   = $this->orderModel::query()
            ->select([
                'orders.*',
            ])
            ->where('machine_id', $machine->machine_id)
            ->find($id);

        if ($order->payment_status === 'paid') {
            return redirect()->route('machine.index');
        }

        $order->payment_status = 'paid';
        $order->service_fee_2 = 0;
        if ($order->order_type == 1) {
            $order->service_fee_2 = $order->total_fee * 0.05;
        }
        $order->save();


        $machine->wallet += $order->allowance;
        $machine->service_charge += $order->service_fee_2;
        $machine->save();

        return redirect()->route('machine.show', ['id' => $id])->with('success', 'Xác nhận đã thanh toán thành công');
    }

    /**
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        $machine = auth('machine')->user();
        $services   = $this->serviceModel->findsByActive();
        $areas      = $this->areaModel->findsByActive();

        return view('fe.machine.create', [
            'services'      => $services,
            'areas'         => $areas,
            'number_areas'  => 10,
            'number_bottle' => 1,
            'discount_fee'  => 0,
            'service_fee'   => Config::getConfig('service_fee', 0)
        ]);
    }

    /**
     * @param OrderRequestByMachine $orderRequest
     * @return RedirectResponse
     */
    public function store(OrderRequestByMachine $orderRequest): RedirectResponse
    {
        $areaId = $orderRequest->post('area_id');
        $area   = $this->areaModel::query()->find($areaId);

        $machine     = auth('machine')->user();
        $serviceFee = Config::getConfig('service_fee', 0);
        $plotOfLand = $orderRequest->post('plot_of_land');
        $cus_phone = $orderRequest->post('cus_phone');
        $cus_name = $orderRequest->post('cus_name');
        $cus_address = $orderRequest->post('cus_address');
        $plotOfBottle = $totalFee = 0;

        if ($areaId == 2) {
            $plotOfBottle = $orderRequest->post('plot_of_bottle');
            $totalFee   = $serviceFee + ($plotOfBottle * $area->fee);
        } else {
            $totalFee   = $serviceFee + ($plotOfLand * $area->fee);
        }


        if ($order = $this->orderModel::query()->create([
            'farmer_id'         => 1,
            'appointment_date'  => date('Y-m-d H:i:s', strtotime($orderRequest->post('appointment_date'))),
            'noted'             => $orderRequest->post('noted'),
            'service_id'        => $orderRequest->post('service_id'),
            'area_id'           => $areaId,
            'unfinished'        => 'unfinished',
            'machine_id'        => $machine->machine_id,
            'plot_of_land'      => $plotOfLand,
            'plot_of_bottle'    => $plotOfBottle,
            'service_fee'       => $serviceFee,
            'total_fee'         => $totalFee,
            'cus_phone'         => $cus_phone,
            'cus_address'       => $cus_address,
            'cus_name'          => $cus_name,
            'order_type'        => 2,
        ])) {
            $totalEstimatedCost = $this->orderHelper->totalEstimatedCost(['allowance' => 0], $totalFee);

            $order->allowance   = $totalEstimatedCost['allowance'];
            $order->wallet      = $totalEstimatedCost['wallet'];
            $order->save();
        }

        return redirect()->route('machine.index')->with('success', 'Đặt lịch thành công');
    }

}
