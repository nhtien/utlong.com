<?php

namespace App\Http\Controllers\FE;

use App\Models\Post;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * @var Post
     */
    protected Post $mainModel;


    public function __construct(Post $mainModel)
    {
        $this->mainModel = $mainModel;
    }


    /**
     * @param $id
     * @param $slug
     * @return View|Factory|RedirectResponse|Application
     */
    public function show($id, $slug): View|Factory|RedirectResponse|Application
    {
        if (!$post = $this->mainModel::parentQuery()->isActivated()->find($id)) {
            return redirect()->route('home');
        }

        return view('fe.post.show');
    }
}
