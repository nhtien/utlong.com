<?php

namespace App\Http\Controllers\FE;

use App\Models\Ward;
use Illuminate\Http\JsonResponse;

class WardController extends Controller
{

    public function __construct(public Ward $wardModel)
    {
        parent::__construct();
    }


    /**
     * @param $province_id
     * @return JsonResponse
     */
    public function findsWardByDistrict($district_id): JsonResponse
    {
        $wards = $this->wardModel->findsWardByDistrict($district_id);
        return response()->json($wards);
    }
}
