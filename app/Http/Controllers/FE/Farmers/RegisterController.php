<?php

namespace App\Http\Controllers\FE\Farmers;

use App\Http\Controllers\FE\Controller;
use App\Http\Requests\FarmerRequest;
use App\Models\Farmer;
use App\Models\Province;
use App\Models\Ward;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /**
     * @var string
     */
    private string $loginForm = '/nong-dan/dang-nhap.html';


    public function __construct(
        public Province $provinceModel,
        public Farmer $farmerModel,
        public Ward $wardModel
    )
    {
        parent::__construct();
        $this->middleware('guest:farmer');
    }


    public function register(FarmerRequest $request): Redirector|RedirectResponse|Application
    {
        $farmer = $this->farmerModel::query()->create([
            'full_name'     => $request->post('full_name'),
            'phone_number'  => $request->post('phone_number'),
            'area'          => $request->post('area'),
            'allowance'     => $this->farmerModel::ALLOWANCE,
            'status'        => 'activated',
            'address'       => $request->post('address_detail'),
            'ward_id'       => $request->post('ward'),
            'province_id'   => $request->post('province'),
            'district_id'   => $request->post('district'),
            'password'      => $request->post('password'),
            'referral_code' => $request->post('referral_code'),
        ]);

        Auth::guard('farmer')->logout();
        Auth::guard('farmer')->loginUsingId($farmer->farmer_id);

        return redirect()->route('farmers.homepage')/*->with('success', 'Tạo tài khoản thành công, hãy đăng nhập !')*/;
    }

    /**
     * @return Application|Factory|View
     */
    public function registerForm()
    {
        $provinces = $this->provinceModel->getAll();
        $wards = $this->wardModel->findsWardByDistrict(637);
        return view('fe.farmer.register_v2',
        [
            'provinces' => $provinces,
            'wards' => $wards,
        ]);
    }

}
