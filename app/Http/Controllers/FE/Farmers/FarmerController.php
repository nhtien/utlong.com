<?php

namespace App\Http\Controllers\FE\Farmers;

use App\Http\Controllers\FE\Controller;
use App\Http\Requests\OrderRequest;
use App\Models\Area;
use App\Models\Config;
use App\Models\Farmer;
use App\Models\Order;
use App\Models\Service;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Helpers\Order as OrderHelper;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class FarmerController extends Controller
{
    public function __construct(
        public Order $orderModel,
        public Service $serviceModel,
        public Area $areaModel,
        public OrderHelper $orderHelper
    )
    {
        parent::__construct();
    }

    /**
     * @return Application|Factory|View
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index()
    {
        $farmerId   = auth('farmer')->user()->farmer_id;
        $allowance = auth('farmer')->user()->allowance;
        $status     = request()?->get('status', 'unfinished');
        $orders = $this->orderModel::query()
            ->select([
                'orders.*',
                'services.name as service_name',
                'areas.name as area_name'
            ])
            ->join('services', 'services.service_id', 'orders.service_id')
            ->join('areas', 'areas.area_id', 'orders.area_id')
            ->when($status !== null, function ($query) use ($status) {
                $query->where('orders.status', $status);
            })
            ->where('farmer_id', $farmerId)
            ->orderBy('appointment_date', 'asc')
            ->paginate();
        return \view('fe.farmer.index_v2', [
            'orders'    => $orders,
            'ordStatus' => $this->orderModel::STATUS,
            'status'    => $status,
            'allowance' => $allowance
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        $services   = $this->serviceModel->findsByActive();
        $areas      = $this->areaModel->findsByActive();
        $farmer     = auth('farmer')->user();
        $discountFee = min($this->orderHelper->getMaxDiscountFee(), $farmer->allowance);
        return view('fe.farmer.create', [
            'services'      => $services,
            'areas'         => $areas,
            'number_areas'  => $farmer->area,
            'number_bottle' => ceil($farmer->area/10),
            'discount_fee'  => $discountFee,
            'service_fee'   => Config::getConfig('service_fee', 0)
        ]);
    }

    /**
     * @param OrderRequest $orderRequest
     * @return RedirectResponse
     */
    public function store(OrderRequest $orderRequest): RedirectResponse
    {
        $areaId = $orderRequest->post('area_id');
        $area   = $this->areaModel::query()->find($areaId);
        /**
         * @var Farmer $farmer
         */

        $farmer     = auth('farmer')->user();
        $serviceFee = Config::getConfig('service_fee', 0);
        $plotOfLand = $orderRequest->post('plot_of_land');
        $plotOfBottle = $totalFee = 0;

        if ($areaId == 2) {
            $plotOfBottle = $orderRequest->post('plot_of_bottle');
            $totalFee   = $serviceFee + ($plotOfBottle * $area->fee);
        } else {
            $totalFee   = $serviceFee + ($plotOfLand * $area->fee);
        }


        if ($order = $this->orderModel::query()->create([
            'farmer_id'         => $farmer->farmer_id,
            'appointment_date'  => date('Y-m-d H:i:s', strtotime($orderRequest->post('appointment_date'))),
            'noted'             => $orderRequest->post('noted'),
            'service_id'        => $orderRequest->post('service_id'),
            'area_id'           => $areaId,
            'unfinished'        => 'unfinished',
            'machine_id'        => 2,
            'plot_of_land'      => $plotOfLand,
            'plot_of_bottle'    => $plotOfBottle,
            'service_fee'       => $serviceFee,
            'total_fee'         => $totalFee,
        ])) {
            $totalEstimatedCost = $this->orderHelper->totalEstimatedCost(['allowance' => $farmer->allowance], $totalFee);

            $farmer->allowance -= $totalEstimatedCost['allowance'];
            $farmer->save();

            $order->allowance   = $totalEstimatedCost['allowance'];
            $order->wallet      = $totalEstimatedCost['wallet'];
            $order->save();
        }

        return redirect()->route('farmer.index')->with('success', 'Đặt lịch thành công');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $farmer = auth('farmer')->user();
        $order = $this->orderModel::query()->find($id);
        // check auth
        $this->authorize('update-order-farmer', $order);
        if ($order->status === 'complete') {
            abort(403);
        }
        $services   = $this->serviceModel->findsByActive();
        $areas      = $this->areaModel->findsByActive();
        $discountFee = min($this->orderHelper->getMaxDiscountFee(), $farmer->allowance);
        return view('fe.farmer.edit', [
            'order'         => $order,
            'services'      => $services,
            'areas'         => $areas,
            'number_areas'  => $farmer->area,
            'discount_fee'  => $discountFee,
            'service_fee'   => Config::getConfig('service_fee', 0)
        ]);
    }

    /**
     * @param OrderRequest $orderRequest
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(OrderRequest $orderRequest)
    {
        $ord_id = $orderRequest->post('ord_id');
        $order  = $this->orderModel::query()->find($ord_id);
        /**
         * @var Farmer $farmer
         */
        $farmer = auth('farmer')->user();

        // check auth
        $this->authorize('update-order-farmer', $order);
        if ($order->status === 'complete') {
            abort(403);
        }

        $areaId = $orderRequest->post('area_id', null);
        $area   = $this->areaModel::query()->find($areaId);
        $serviceFee = $order->service_fee;
        $plotOfLand = $orderRequest->post('plot_of_land');
        $plotOfBottle = $totalFee = 0;

        if ($areaId == 2) {
            $plotOfBottle = $orderRequest->post('plot_of_bottle');
            $totalFee   = $serviceFee + ($plotOfBottle * $area->fee);
        } else {
            $totalFee   = $serviceFee + ($plotOfLand * $area->fee);
        }

        if ($order->update([
            'farmer_id'         => $farmer->farmer_id,
            'appointment_date'  => date('Y-m-d H:i:s', strtotime($orderRequest->post('appointment_date'))),
            'noted'             => $orderRequest->post('noted'),
            'service_id'        => $orderRequest->post('service_id'),
            'area_id'           => $areaId,
            'plot_of_land'      => $plotOfLand,
            'plot_of_bottle'    => $plotOfBottle,
            'total_fee'         => $totalFee,
            'wallet'            => $totalFee - $order->allowance,
        ])) {
            return redirect()->route('farmer.index')->with('success', 'Cập nhật thành công');
        }

        return redirect()->route('farmer.index')->with('error', 'Cập nhật thất bại');
    }
}
