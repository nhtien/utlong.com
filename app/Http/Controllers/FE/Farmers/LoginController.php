<?php

namespace App\Http\Controllers\FE\Farmers;

use App\Http\Controllers\FE\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /**
     * @var string
     */
    private string $loginForm = '/nong-dan/dang-nhap.html';


    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest:farmer');
    }

    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function login(Request $request): Redirector|RedirectResponse|Application
    {
        /**
         * Validate the form data
         */
        $this->validate($request, [
            'phone_number'  => 'bail|required',
            'password'  => 'bail|required'
        ]);

        /**
         * Verify user
         */
        $credentials = $request->only('phone_number', 'password');
        $rememberMe  = (bool)$request->get('remember-me');
        if (Auth::guard('farmer')->attempt(array_merge($credentials, ['status' => 'activated']), $rememberMe)) {
            $request->session()->regenerate();
            return redirect()->route('farmers.homepage');
        }

        return redirect($this->loginForm)->with('error', 'Số điện thoại hoặc mật khẩu không chính xác !');
    }

    /**
     * @return Application|Factory|View
     */
    public function loginForm(): View|Factory|Application
    {
        return view('fe.layouts.login',
        [
            'action'    => '/nong-dan/dang-nhap.html',
            'image'     => '/public/fe/login/images/Giong-lua-lai-KC06-1.jpg',
            'register'  => '/nong-dan/dang-ky.html',
            'name'      => 'Nông dân'
        ]);
    }

}
