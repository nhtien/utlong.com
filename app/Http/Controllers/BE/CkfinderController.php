<?php

namespace App\Http\Controllers\BE;


use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class CkfinderController extends Controller
{
    /**
     * @return View|Factory|Application
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function show(): View|Factory|Application
    {
        $type   = request()->get('type', 'undefined');
        $folder = request()->get('folder', '');
        return view('be.ckfinder.show', ['type' => $type, 'folder' => $folder]);
    }
}
