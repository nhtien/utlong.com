<?php

namespace App\Http\Controllers\BE;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected string $backendName;

    public function __construct()
    {
        $this->backendName = config('constants.route.backend');
        View::share('backendName', $this->backendName);
    }
}
