<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Middleware\CheckRootAccount;
use App\Http\Requests\MenuRequest;
use App\Models\Menu;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class MenuController extends Controller
{
    /**
     * @var Menu
     */
    public Menu $menuModel;

    /**
     * @param Menu $menuModel
     */
    public function __construct(Menu $menuModel)
    {
        parent::__construct();
        $this->middleware(CheckRootAccount::class);
        $this->menuModel = $menuModel;
    }

    /**
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $menus = $this->menuModel::parentQuery()->paginate();
        return view('be.menu.index', ['menus' => $menus]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        return view('be.menu.create', [
            'menuStatus'     => $this->menuModel::STATUS
        ]);
    }


    /**
     * @param MenuRequest $menuRequest
     * @return RedirectResponse
     */
    public function store(MenuRequest $menuRequest): RedirectResponse
    {
        $data = $this->menuModel->getDataFields($menuRequest->post());
        $this->menuModel::parentQuery()->create($data);
        return redirect()->route('be.menu.index')->with('success', config('message.store_successful'));
    }


    /**
     * @param $id
     * @return false
     */
    public function show($id): bool
    {
        return false;
    }


    /**
     * @return View|Factory|Application|RedirectResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|Application|RedirectResponse
    {
        $id = \request()->get('id', 0);
        if (!$menu = $this->menuModel::parentQuery()->find($id)) {
            throw new NoResultFoundException();
        }
        return \view('be.menu.edit', [
            'menu'          => $menu,
            'menuStatus'    => $this->menuModel::STATUS
        ]);
    }


    /**
     * @param MenuRequest $menuRequest
     * @return RedirectResponse
     */
    public function update(MenuRequest $menuRequest): RedirectResponse
    {
        $menu = $this->menuModel::parentQuery()->find(\request()->post('menu_id'));
        $data = $this->menuModel->getDataFields($menuRequest->post());
        $menu->update($data);
        return redirect()->route('be.menu.index')->with('success', config('message.update_successful'));
    }


    /**
     * @param $id
     * @return false
     */
    public function destroy($id): bool
    {
        return false;
    }
}
