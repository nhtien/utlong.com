<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\ConfigRequest;
use App\Models\Config;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ConfigController extends Controller
{
    /**
     * @var Config
     */
    public Config $configModel;

    /**
     * @param Config $configModel
     */
    public function __construct(Config $configModel)
    {
        parent::__construct();
        $this->configModel = $configModel;
    }

    /**
     * @return Factory|View|Application
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(): Factory|View|Application
    {
        $configs = $this->configModel->searching();
        return view('be.config.index', [
            'configs'       => $configs,
            'confStatus'    => $this->configModel::STATUS,
            'confType'      => $this->configModel::TYPE,
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        return view('be.config.create', [
            'confStatus' => $this->configModel::STATUS,
            'confType'   => $this->configModel::TYPE
        ]);
    }


    /**
     * @param ConfigRequest $configRequest
     * @return RedirectResponse
     */
    public function store(ConfigRequest $configRequest): RedirectResponse
    {
        $data = $this->configModel->getDataFields($configRequest->post());
        $this->configModel::query()->create($data);
        return redirect()->route('be.config.index')->with('success', \config('message.store_successful'));
    }

    /**
     * @return false
     */
    public function show(): bool
    {
        return false;
    }

    /**
     * @return Application|Factory|View|RedirectResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|RedirectResponse|Application
    {
        $id = \request()->get('id', 0);
        if (!$config = $this->configModel::query()->find($id)) {
            throw new NoResultFoundException();
        }

        return \view('be.config.edit', [
            'config'       => $config,
            'confStatus'   => $this->configModel::STATUS,
            'confType'     => $this->configModel::TYPE,
        ]);
    }

    /**
     * @param ConfigRequest $configRequest
     * @return RedirectResponse
     */
    public function update(ConfigRequest $configRequest): RedirectResponse
    {
        $config = $this->configModel::query()->find(\request()->post('cfg_id'));
        $data   = $this->configModel->getDataFields($configRequest->post(), ['cfg_key']);
        $config->update($data);
        return redirect()->route('be.config.index')->with('success', \config('message.update_successful'));
    }

    /**
     * @return RedirectResponse
     */
    public function destroy(): RedirectResponse
    {
        $id = \request()->post('id', 0);
        $this->configModel::query()->find($id)->delete();
        return redirect()->back()->with('success', \config('message.destroy_successful'));
    }
}
