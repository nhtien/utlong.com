<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class UserController extends Controller
{
    /**
     * @var User
     */
    protected User $userModel;

    /**
     * @var UserGroup
     */
    protected UserGroup $userGroupModel;

    public function __construct(User $userModel, UserGroup $userGroupModel)
    {
        parent::__construct();
        $this->userModel = $userModel;
        $this->userGroupModel = $userGroupModel;
    }

    /**
     * @return View|Factory|Application
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(): View|Factory|Application
    {
        $users      = $this->userModel->searching();
        $userGroups = $this->userGroupModel::query()->get();
        return view('be.user.index', [
            'users'         => $users,
            'userGroups'    => $userGroups,
            'usrStatus'     => $this->userModel::STATUS
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Application|Factory|View
     */
    public function create(): Factory|View|Application
    {
        $userGroups = $this->userGroupModel::query()->get();
        return view('be.user.create', [
            'userGroups'    => $userGroups,
            'usrStatus'     => $this->userModel::STATUS
        ]);
    }

    /**
     * @param UserRequest $userRequest
     * @return RedirectResponse
     */
    public function store(UserRequest $userRequest): RedirectResponse
    {
        $data = $this->userModel->getDataFields($userRequest->post(), ['email_verified_at', 'remember_token']);
        $this->userModel::query()->create($data);
        return redirect()->route('be.users.index');
    }

    /**
     * @return false
     */
    public function show(): bool
    {
        return false;
    }

    /**
     * @return View|Factory|Application
     * @throws NoResultFoundException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function edit(): View|Factory|Application
    {
        $id = \request()->get('id', 0);
        if (!$user = $this->userModel::query()->find($id)) {
            throw new NoResultFoundException();
        }
        $userGroups = $this->userGroupModel::query()->get();
        return \view('be.user.edit', [
            'user'          => $user,
            'userGroups'    => $userGroups,
            'usrStatus'     => $this->userModel::STATUS
        ]);
    }


    /**
     * @param UserRequest $userRequest
     * @return RedirectResponse
     */
    public function update(UserRequest $userRequest): RedirectResponse
    {
        $user = $this->userModel::query()->find(\request()->post('usr_id'));
        $data = $this->userModel->getDataFields(
                                                    $userRequest->post(),
                                                    ['email_verified_at', 'username', 'remember_token']
                                                );
        if (!$data['password']) {
            unset($data['password']);
        }
        $user->update($data);
        return redirect()->route('be.users.index')->with('success', \config('message.update_successful'));
    }


    /**
     * @return RedirectResponse
     */
    public function destroy(): RedirectResponse
    {
        $id = \request()->post('id', 0);
        $this->userModel::query()->find($id)->delete();
        return redirect()->back()->with('success', \config('message.destroy_successful'));
    }

    /**
     * @return Application|Factory|View|RedirectResponse
     */
    public function editProfile(): View|Factory|RedirectResponse|Application
    {
        $user = auth()->user();
        return \view('be.user.edit-profile', [
            'user'          => $user,
            'usrStatus'     => $this->userModel::STATUS
        ]);
    }

    /**
     * @param UserRequest $userRequest
     * @return RedirectResponse
     */
    public function updateProfile(UserRequest $userRequest): RedirectResponse
    {
        $data = $this->userModel->getDataFields(
                                                    $userRequest->post(),
                                                    ['email_verified_at', 'remember_token', 'username', 'status', 'usrgroup_id']
                                                );
        if (!$data['password']) {
            unset($data['password']);
        }
        $userID = auth()->user()->user_id;
        $this->userModel::parentQuery()->where(['user_id' => $userID])->first()->update($data);
        return redirect()->route('dashboard-backend')->with('success', \config('message.update_successful'));;
    }
}
