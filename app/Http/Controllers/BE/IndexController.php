<?php

namespace App\Http\Controllers\BE;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class IndexController extends Controller
{

    public function __construct(public User $userModel)
    {
        parent::__construct();
    }

    /**
     * @return Factory|View|Application
     */
    public function dashboard(): Factory|View|Application
    {
        return view('be.index.dashboard', [
            'totalUsers' => $this->userModel->totalUsers()
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function cache(): View|Factory|Application
    {
        return \view('be.index.cache');
    }
}
