<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewCustomerHasRegisteredEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    /**
     * @var User
     */
    public User $user;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $userModel)
    {
        $this->user = $userModel;
    }
}
