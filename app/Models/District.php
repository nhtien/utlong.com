<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class District extends BaseModel
{
    use HasFactory;

    protected $table = 'districts';
    protected $primaryKey = 'id';

    public const CREATED_AT = null;
    public const UPDATED_AT = null;


    public function getAll(): Collection|array
    {
        return self::query()->orderBy('name', 'asc')->get();
    }


    public function findsDistrictByProvince($provinceID): Collection|array
    {
        return self::query()->where('province_id', $provinceID)->orderBy('name', 'asc')->get();
    }

}
