<?php

namespace App\Models;

use App\Models\Scopes\UserGroup\IgnoreRootGroup;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Province extends BaseModel
{
    use HasFactory;

    protected $table = 'provinces';
    protected $primaryKey = 'id';

    public const CREATED_AT = null;
    public const UPDATED_AT = null;


    public function getAll(): Collection|array
    {
        return self::query()->orderBy('name', 'asc')->get();
    }


}
