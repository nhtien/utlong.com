<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Area extends BaseModel
{
    use HasFactory;

    protected $table = 'areas';
    protected $primaryKey = 'area_id';

    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';


    public function findsByActive(): Collection|array
    {
        return self::query()->where('status', 'activated')->get();
    }


}
