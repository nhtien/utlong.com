<?php

namespace App\Models;

use App\Casts\DateFormat;
use App\Casts\Password;
use App\Events\WriteModelLogEvent;
use App\Traits\Models\Attributes\UserAttribute;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Relationships\UserRelationship;
use App\Traits\Models\Scopes\UserScope;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class User
 * @property int user_id
 * @property string name
 * @property string username
 * @property string email
 * @property string avatar
 * @property string email_verified_at
 * @property string password
 * @property string status
 * @property string remember_token
 * @property string created_at
 * @property string updated_at
 * @property int usrgroup_id
 * @package App\Models
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use InputFilter, UserScope, UserRelationship, UserAttribute;

    protected $table = 'users';

    protected $perPage = 50;

    protected $primaryKey = 'user_id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive'
    ];

    const ALIAS = [
        'user_id'       => 'usr_id',
        'name'          => 'usr_full_name',
        'username'      => 'usr_username',
        'email'         => 'usr_email',
        'status'        => 'usr_status',
        'usrgroup_id'   => 'usr_group_id',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'avatar',
        'password',
        'status',
        'usrgroup_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password'          => Password::class,
        'created_at'        => DateFormat::class,
        'updated_at'        => DateFormat::class,
    ];

    /**
     * @return void
     */
    public static function boot() {
        parent::boot();

        static::deleted(function($item) {
            event(new WriteModelLogEvent($item));
        });
    }

    /**
     * @return Builder
     */
    public static function query(): Builder
    {
        return parent::query()->notRootUser(); // TODO: Change the autogenerated stub
    }

    /**
     * @return Builder
     */
    public static function parentQuery(): Builder
    {
        return parent::query();
    }


    /**
     * @return LengthAwarePaginator
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function searching(): LengthAwarePaginator
    {
        $keyword = \request()->get('keyword', '');
        return QueryBuilder::for($this)
            ->allowedFilters([
                AllowedFilter::exact(config('aliases-model.users.usrgroup_id'), 'usrgroup_id'),
                AllowedFilter::exact(config('aliases-model.users.status'), 'status')
            ])
            ->with('userGroup')
            ->defaultSort("-user_id")
            ->allowedSorts([
                AllowedSort::field(config('aliases-model.users.user_id'), 'user_id'),
            ])
            ->when(!empty($keyword), function ($q) use ($keyword){
                $q->where(function ($q) use ($keyword){
                    $q->orWhere('name', 'like', "%$keyword%")
                        ->orWhere('email', 'like', "%$keyword%")
                        ->orWhere('username', 'like', "%$keyword%");
                });
            })
            ->paginate();
    }

    /**
     * @return int
     */
    public function totalUsers(): int
    {
        return self::query()->select('user_id')->count();
    }
}
