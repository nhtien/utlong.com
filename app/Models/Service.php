<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Service extends BaseModel
{
    use HasFactory;

    protected $table = 'services';
    protected $primaryKey = 'service_id';

    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';


    public function findsByActive(): Collection|array
    {
        return self::query()->where('status', 'activated')->get();
    }


}
