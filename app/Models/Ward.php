<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ward extends BaseModel
{
    use HasFactory;

    protected $table = 'wards';
    protected $primaryKey = 'id';

    public const CREATED_AT = null;
    public const UPDATED_AT = null;


    public function getAll(): Collection|array
    {
        return self::query()->orderBy('name', 'asc')->get();
    }


    public function findsWardByDistrict($districtId): Collection|array
    {
        return self::query()->where('district_id', $districtId)->orderBy('name', 'asc')->get();
    }

}
