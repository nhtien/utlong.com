<?php

namespace App\Models;

use App\Traits\Models\Attributes\ConfigAttribute;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Scopes\ConfigScope;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Support\Facades\Cache;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;


/**
 * Class Config
 * @property int conf_id
 * @property string conf_name
 * @property string conf_description
 * @property string conf_key
 * @property string conf_value
 * @property string conf_status
 * @property string conf_type
 * @package App\Models
 */
class Config extends BaseModel
{
    use HasFactory, InputFilter, ConfigAttribute, ConfigScope;

    protected $perPage = 50;

    protected $table = 'configs';

    protected $primaryKey = 'conf_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive'
    ];

    const TYPE = [
        'int'       => 'Int',
        'float'     => 'Float',
        'string'    => 'String',
        'array'     => 'Array',
        'json'      => 'Json',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'conf_name',
        'conf_description',
        'conf_key',
        'conf_value',
        'conf_status',
        'conf_type',
    ];

    /**
     * @return Builder
     */
    public static function query(): Builder
    {
        return parent::query();
    }

    /**
     * @return LengthAwarePaginator
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function searching(): LengthAwarePaginator
    {
        $keyword = \request()->get('keyword', '');
        return QueryBuilder::for($this)
            ->allowedFilters([
                AllowedFilter::exact(config('aliases-model.configs.conf_id'), 'conf_id'),
                AllowedFilter::exact(config('aliases-model.configs.conf_status'), 'conf_status')
            ])
            ->defaultSort("-conf_id")
            ->allowedSorts([
                AllowedSort::field(config('aliases-model.configs.conf_id'), 'conf_id'),
            ])
            ->when(!empty($keyword), function ($q) use ($keyword){
                $q->where(function ($q) use ($keyword){
                    $q->orWhere('conf_name', 'like', "%$keyword%")
                        ->orWhere('conf_description', 'like', "%$keyword%")
                        ->orWhere('conf_key', 'like', "%$keyword%")
                        ->orWhere('conf_value', 'like', "%$keyword%");
                });
            })
            ->paginate();
    }

    /**
     * @param $key
     * @param string $default
     * @return HigherOrderBuilderProxy|mixed|string
     */
    public static function getConfig($key, string $default = ''): mixed
    {
        $keyCache   = 'configs__' . $key;
        if (Cache::has($keyCache)) {
            $data = Cache::get($keyCache);
        }else{
            $config = self::query()
                            ->select('conf_value', 'conf_type')
                            ->key($key)
                            ->isActivated()
                            ->first();
            if (optional($config)->count() == 0) {
                return $default;
            }
            $data = [
                'value'      => $config->conf_value,
                'data_types' => $config->conf_type
            ];
            Cache::put($keyCache, $data, now()->addMinutes(15));
        }
        return convert_string($data['value'], $data['data_types']) ?? $default;
    }
}
