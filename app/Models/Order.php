<?php

namespace App\Models;

use App\Casts\DateFormat;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends BaseModel
{
    use HasFactory;

    protected $perPage = 50;
    protected $table = 'orders';
    protected $primaryKey = 'ord_id';

    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';

    const STATUS = [
        'complete' => 'Đã hoàn thành',
        'unfinished' => 'Chưa hoàn thành',
    ];

    protected $casts = [
        'appointment_date' => DateFormat::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'farmer_id',
        'machine_id',
        'appointment_date',
        'noted',
        'service_id',
        'area_id',
        'status',
        'allowance',
        'wallet',
        'service_fee',
        'service_fee_2',
        'plot_of_land',
        'plot_of_bottle',
        'payment_status',
        'total_fee',
        'cus_name',
        'cus_phone',
        'cus_address',
        'order_type',
    ];

    public function findsByFarmer($farmerId): Collection|array
    {
        return self::query()->where('farmer_id', $farmerId)->orderBy('ord_id', 'asc')->get();
    }


}
