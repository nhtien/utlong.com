<?php

namespace App\View\Components\FE;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use function view;

class Alert extends Component
{
    /**
     * The alert type.
     *
     * @var string
     */
    public string $type;

    /**
     * @var string
     */
    public string $message;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($type, $message)
    {
        $this->type     = $type;
        $this->message  = $message;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|Closure|string
     */
    public function render(): View|string|Closure
    {
        return view('components.alert');
    }
}
