<?php

namespace App\View\Components\BE;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Searching extends Component
{
    /**
     * @var string
     */
    public string $title;

    /**
     * @var string
     */
    public string $formAction;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $formAction)
    {
        $this->title = $title;
        $this->formAction = $formAction;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|Closure|string
     */
    public function render(): View|string|Closure
    {
        return view('components.be.searching');
    }
}
