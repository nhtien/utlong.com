<?php

namespace App\View\Components\BE\Elements;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class PopupCkfinder extends Component
{
    /**
     * @var string
     */
    public string $type;

    /**
     * @var string
     */
    public string $folder;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($type, $folder)
    {
        $this->type     = $type;
        $this->folder   = $folder;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|Closure|string
     */
    public function render(): View|string|Closure
    {
        return view('components.be.elements.popup-ckfinder');
    }
}
