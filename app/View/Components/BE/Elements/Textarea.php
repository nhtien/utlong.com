<?php

namespace App\View\Components\BE\Elements;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Textarea extends Component
{
    /**
     * @var string
     */
    public string $id;
    /**
     * @var string
     */
    public string $content;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id, $content)
    {
        $this->id       = $id;
        $this->content  = $content;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        return view('components.be.elements.textarea');
    }
}
