<?php

namespace App\View\Components\BE;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Menu extends Component
{
    /**
     * @var array
     */
    public array $parentMenus;

    /**
     * @var array
     */
    public array $childrenMenus;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        /**
         * @var \App\Models\Menu $MenuModel
         */
        $menuModel           = app('MenuModel');
        $this->parentMenus   = $menuModel->getMenuByUser('parent')->toArray();
        $this->childrenMenus = collect($menuModel->getMenuByUser('children')->toArray())->groupBy('parent')->toArray();
    }

    /**
     * @return View|string|Closure
     */
    public function render(): View|string|Closure
    {
        return view('components.be.menu');
    }
}
