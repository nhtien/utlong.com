<?php

namespace App\Traits\Models\Scopes;

trait MenuFrontendScope
{
    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }


    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotRootItem($query): mixed
    {
        return $query->where('id', '<>', 1);
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeStatus($query, $value): mixed
    {
        return $query->where('status', $value);
    }


    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeSpecial($query, $value): mixed
    {
        return $query->where('special', $value);
    }
}
