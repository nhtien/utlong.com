<?php

namespace App\Traits\Models\Scopes;

trait ModelLogScope
{
    /**
     * @param $query
     * @param $type
     * @return mixed
     */
    public function scopeType($query, $type): mixed
    {
        return $query->where('type', $type);
    }


}
