<?php

namespace App\Traits\Models\Scopes;

trait ConfigScope
{
    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsActivated($query): mixed
    {
        return $query->where('conf_status', 'activated');
    }

    /**
     * @param $query
     * @param $status
     * @return mixed
     */
    public function scopeStatus($query, $status): mixed
    {
        return $query->where('conf_status', $status);
    }

    /**
     * @param $query
     * @param $type
     * @return mixed
     */
    public function scopeType($query, $type): mixed
    {
        return $query->where('conf_type', $type);
    }

    /**
     * @param $query
     * @param $key
     * @return mixed
     */
    public function scopeKey($query, $key): mixed
    {
        return $query->where('conf_key', $key);
    }
}
