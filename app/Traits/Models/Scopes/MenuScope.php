<?php

namespace App\Traits\Models\Scopes;

trait MenuScope
{
    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsParent($query): mixed
    {
        return $query->where('parent', 0);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotParent($query): mixed
    {
        return $query->where('parent', '<>', 0);
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeParent($query, $value): mixed
    {
        return $query->where('parent', $value);
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeController($query, $value): mixed
    {
        return $query->where('controller', $value);
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeAction($query, $value): mixed
    {
        return $query->where('action', $value);
    }
}
