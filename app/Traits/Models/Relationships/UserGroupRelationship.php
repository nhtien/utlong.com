<?php
namespace App\Traits\Models\Relationships;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\User;

trait UserGroupRelationship
{
    /**
     * @return HasMany
     */
    public function user(): HasMany
    {
        return $this->hasMany(User::class, 'usrgroup_id', 'usrgroup_id');
    }
}
