<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait ConfigAttribute
{
    /**
     * @return Attribute
     */
    public function confName():Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    /**
     * @return Attribute
     */
    public function confDescription():Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    /**
     * @return Attribute
     */
    public function confKey():Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }

    /**
     * @return Attribute
     */
    public function confValue():Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    /**
     * @return Attribute
     */
    public function confType(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['int', 'float', 'string', 'array', 'json']) ? $value : 'string',
        );
    }


    /**
     * @return Attribute
     */
    public function confStatus(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? $value : 'inactive',
        );
    }

}
