<?php

namespace App\Traits\Models\Attributes;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait UserAttribute
{
    /**
     * @return Attribute
     */
    public function name(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    /**
     * @return Attribute
     */
    public function username(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }

    /**
     * @return Attribute
     */
    public function status(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? $value : 'inactive',
        );
    }
}
