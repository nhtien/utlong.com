<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait MenuFrontendAttribute
{
    /**
     * @return Attribute
     */
    public function code():Attribute
    {
        return new Attribute(
            get: fn ($value) => strtolower($value),
            set: fn ($value) => strtolower(strip_tags($value)),
        );
    }

    /**
     * @return Attribute
     */
    public function name():Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    /**
     * @return Attribute
     */
    public function nameEn():Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    /**
     * @return Attribute
     */
    public function link():Attribute
    {
        return new Attribute(
            get: fn ($value) => strtolower($value),
            set: fn ($value) => strtolower(strip_tags($value)),
        );
    }

    /**
     * @return Attribute
     */
    public function linkEn():Attribute
    {
        return new Attribute(
            get: fn ($value) => strtolower($value),
            set: fn ($value) => strtolower(strip_tags($value)),
        );
    }

    /**
     * @return Attribute
     */
    public function special():Attribute
    {
        return new Attribute(
            get: fn ($value) => strtolower($value),
            set: fn ($value) => in_array($value, ['default', 'link', 'new', 'popular', 'hot']) ? strtolower($value) : 'default',
        );
    }

    /**
     * @return Attribute
     */
    public function icon():Attribute
    {
        return new Attribute(
            get: fn ($value) => strtolower($value),
            set: fn ($value) => strtolower(strip_tags($value)),
        );
    }

    /**
     * @return Attribute
     */
    public function status(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? strtolower($value) : 'inactive',
        );
    }

}
