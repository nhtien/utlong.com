<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait MenuAttribute
{
    /**
     * @return Attribute
     */
    public function name():Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    /**
     * @return Attribute
     */
    public function controller():Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }

    /**
     * @return Attribute
     */
    public function httpQuery(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }

    /**
     * @return Attribute
     */
    public function action():Attribute
    {
        return new Attribute(
            get: fn ($value) => strtolower($value),
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }


    /**
     * @return Attribute
     */
    public function parent():Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    /**
     * @return Attribute
     */
    public function status(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? $value : 'inactive',
        );
    }


    /**
     * @return Attribute
     */
    public function icon():Attribute
    {
        return new Attribute(
            get: fn ($value) => strtolower($value),
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }

    /**
     * @return Attribute
     */
    public function position():Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }
}
