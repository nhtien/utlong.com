<?php
namespace App\Traits\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Vinkla\Hashids\Facades\Hashids;

trait HashId
{
    /**
     * @return Attribute
     */
    public function encodeId(): Attribute
    {
        return new Attribute(
            get: fn () => encode_hash_id($this->{$this->primaryKey}),
        );
    }

    public function scopeByHashId($query, string $hash)
    {
        $id = count(Hashids::decode($hash)) ? Hashids::decode($hash)[0] : 0;
        return $query->where($this->primaryKey, '=', $id);
    }
}
