<?php

use App\Models\Config;
use App\Models\Menu;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Vinkla\Hashids\Facades\Hashids;

if (! function_exists('get_controller_name')) {
    function get_controller_name(): array|string
    {
        return str_replace('Controller', '', class_basename(Route::current()->controller) );
    }
}

if (! function_exists('get_action_name')) {
    function get_action_name(): array|string
    {
        return \request()->route()->getActionMethod();
    }
}


if (! function_exists('is_root_account')) {
    function is_root_account(): bool
    {
        $user = auth()->guard()->user();
        if (
            in_array(optional($user)->user_id, config('constants.account.root_user_ids')) &&
            in_array(optional($user)->usrgroup_id, config('constants.account.root_group_ids'))
        ) {
            return true;
        }
        return false;
    }
}


/*
|--------------------------------------------------------------------------
| Kiểm tra quyền truy cập hiện tại có được phép hay không
|--------------------------------------------------------------------------
*/
if (! function_exists('check_access_current_action')) {
    function check_access_current_action(): bool
    {
        $auth = auth()->guard();
        if ($auth->check()) {
            $user = $auth->user();
            if (optional($user->userGroup)->status != 'activated') {
                return false;
            }
            $menuIds = optional($user->userGroup)->menu_ids;
            /**
             * @var Menu $menuModel
             */
            $menuModel  = app('MenuModel');
            $menus      = $menuModel->getMenuDontAccess($menuIds);
            if (count($menus) == 0) {
                return true;
            } else {
                $currentAction      = get_action_name();
                $currentController  = get_controller_name();
                $accessIsDenied = collect($menus->toArray())->contains(function ($item) use ($currentAction, $currentController) {
                    $actionName     = preg_replace('/[^A-Za-z0-9]/', '', $item['action']);
                    $controllerName = preg_replace('/[^A-Za-z0-9]/', '', $item['controller']);
                    return strtolower($currentAction) == strtolower($actionName) &&
                           strtolower($currentController) == strtolower($controllerName);
                });
                if ($accessIsDenied == true) {
                    return false;
                }
                return true;
            }
        }

        return false;
    }
}


if (! function_exists('get_user_id')) {
    function get_user_id(): int
    {
        $auth = auth()->guard();
        if ($auth->check()) {
            return $auth->user()->user_id;
        }

        return 0;
    }
}


/*
|--------------------------------------------------------------------------
| Filter aliases: sử dụng khi searching với Query builder package
|--------------------------------------------------------------------------
*/
if (! function_exists('get_column_alias')) {
    function get_column_alias($key)
    {
        return config("aliases-model.{$key}");
    }
}


/*
|--------------------------------------------------------------------------
| Filter aliases: Trả về mảng dữ liệu được phép tương ứng với table
|--------------------------------------------------------------------------
*/
if (! function_exists('get_column_aliases')) {
    function get_column_aliases(string $tableName, array $data): array
    {
        $fields = collect(config("aliases-model.{$tableName}"))->flip();
        return collect($data)->intersectByKeys($fields)->toArray();
    }
}

/*
|--------------------------------------------------------------------------
| Trả về mảng dữ liệu khớp với field của table
|--------------------------------------------------------------------------
*/
if (! function_exists('get_column_matches')) {
    function get_column_matches(string $tableName, array $data): array
    {
        $column  = collect(config("aliases-model.{$tableName}"))->flip()->toArray();
        $matches = collect($data)->intersectByKeys($column)->toArray();
        $final   = [];
        foreach ($matches as $key => $match) {
            if (array_key_exists($key, $column))
                $final[$column[$key]] = $match;
        }
        return $final;
    }
}


if (! function_exists('get_date')) {
    function get_date($timestamp): string
    {
        return date('H:i:s d/m/Y', strtotime($timestamp));
    }
}

if (! function_exists('get_root_group_ids')) {
    function get_root_group_ids()
    {
        return config('constants.account.root_group_ids');
    }
}


if (! function_exists('get_root_user_ids')) {
    function get_root_user_ids()
    {
        return config('constants.account.root_user_ids');
    }
}


if (! function_exists('convert_string')) {
    function convert_string($value, $type)
    {
        return match ($type) {
            "string"    => (string)$value,
            "int"       => (int)$value,
            "float"     => (float)$value,
            "json"      => json_decode($value, 1),
            "array"     => explode('|', $value),
        };
    }
}


if (! function_exists('get_config')) {
    function get_config($key, $default = '')
    {
        return Config::getConfig($key, $default);
    }
}


if (! function_exists('upload_file')) {
    function upload_file($objName, $folderPath, $filename): bool
    {
        if (\request()->hasFile($objName)) {
            \request()->file($objName)->move($folderPath, $filename);
            return true;
        }
        return false;
    }
}


if (! function_exists('create_filename')) {
    function create_filename($objOrString, $extension = 'png'): string
    {
        if (\request()->hasFile($objOrString)) {
            $extension = \request()->file($objOrString)->extension();
            $filename  = Str::replace('.' . \request()->file($objOrString)->getClientOriginalExtension(), '', \request()->file($objOrString)->getClientOriginalName()) ;
            $slug      = Str::slug($filename);
        }else{
            $slug = Str::slug($objOrString);
        }

        return $slug . '-' . time() . '.' . $extension;
    }
}


if (! function_exists('delete_file')) {
    function delete_file( $filePath): bool
    {
        if (file_exists($filePath))
            unlink($filePath);

        return true ;
    }
}

/**
 * ký hiệu theo cấp độ (nested set model)
 */
if (! function_exists('notation_by_level')) {
    function notation_by_level( int $level): string
    {
        $level = ($level > 0) ? $level : 0;
        return str_repeat('|---> ', $level);
    }
}

/*
|--------------------------------------------------------------------------
| Chuyển chuỗi ids thành mảng
| ex: 1||2||3 => [1,2,3]
|--------------------------------------------------------------------------
*/
if (! function_exists('convert_ids_to_array')) {
    function convert_ids_to_array($ids): array
    {
        $ids = is_array($ids) ? $ids : explode('||', $ids);
        return collect($ids)->filter()->all();
    }
}


if (! function_exists('convert_ids_to_json')) {
    function convert_ids_to_json($ids): string
    {
        if (is_json($ids)) {
            return $ids;
        } else {
            $ids = convert_ids_to_array($ids);
            $ids2 = [];
            foreach ($ids as $key => $id) {
                $ids2[is_numeric($key) ? $id : $key] = $id;
            }
            return json_encode($ids2);
        }
    }
}


if (! function_exists('is_json')) {
    function is_json($string): bool
    {
        return is_string($string) && is_array(json_decode($string, true));
    }
}


/*
|--------------------------------------------------------------------------
| Tạo folder name.
| /[\\:*?\"<>|\/]/ => -
| # => @
|--------------------------------------------------------------------------
*/
if (! function_exists('create_folder_name')) {
    function create_folder_name($name): array|string|null
    {
        $name = preg_replace("/[\\:*?\"<>|\/]/", "-", $name);
        $name = str_replace('\\', '-', $name);
        return str_replace('#', '@', $name);
    }
}

if (! function_exists('rename_folder')) {
    function rename_folder(string $from, string $to): bool
    {
        if (!File::isDirectory($from)) {
            return false;
        }

        if (File::moveDirectory($from, $to)) {
            return true;
        }
        return false;
    }
}

if (! function_exists('create_folder')) {
    function create_folder($path, $folderName): bool
    {
        if ($folderName == '' || $folderName == null) {
            return false;
        }

        $path = $path . create_folder_name($folderName);
        if (File::makeDirectory($path, 0777, true, true)) {
            return true;
        }

        return false;
    }
}

/*
|--------------------------------------------------------------------------
| Kiểm tra password có phải hash không
|--------------------------------------------------------------------------
*/
if (! function_exists('password_is_hash')) {
    function password_is_hash($password): bool
    {
        return password_get_info($password)['algoName'] !== 'unknown';
    }
}

/*
|--------------------------------------------------------------------------
| Encode hashId
|--------------------------------------------------------------------------
*/
if (! function_exists('encode_hash_id')) {
    function encode_hash_id($id):string
    {
        return Hashids::encode($id);
    }
}


/*
|--------------------------------------------------------------------------
| Lấy filename nằm trong thư mục
|--------------------------------------------------------------------------
*/
if (! function_exists('get_file_name_in_folder')) {
    function get_file_name_in_folder($path, $fileType = '*'): array
    {
        $files = get_files_in_folder($path, $fileType);
        $filenames = [];
        foreach ($files as $file) {
            $filenames[] = pathinfo($file)['basename'];
        }
        return $filenames;
    }
}

/*
|--------------------------------------------------------------------------
| Lấy files nằm trong thư mục
|--------------------------------------------------------------------------
*/
if (! function_exists('get_files_in_folder')) {
    function get_files_in_folder($path, $fileType = '*'): array
    {
        $files = $fileType == '*' ? glob($path . '/*') : glob($path . "/*.{$fileType}");
        $filePaths = [];
        foreach($files as $filePath){
            if(is_file($filePath)){
                $filePaths[] = $filePath;
            }
        }
        return $filePaths;
    }
}

/*
|--------------------------------------------------------------------------
| Remove những file cũ hơn số ngày nào đó
|--------------------------------------------------------------------------
*/
if (! function_exists('delete_files_older_than_days')) {
    function delete_files_older_than_days(array $files, int $day):array
    {
        $now = time();
        $filesHasRemove = [];
        foreach ($files as $file) {
            if (is_file($file)) {
                if ($now - filemtime($file) >= 60 * 60 * 24 * $day) { // days
                    if (unlink($file)) {
                        $filesHasRemove[] = $file;
                    }
                }
            }
        }
        return $filesHasRemove;
    }
}


/**
 *
 * Chuyển đổi chuỗi kí tự thành dạng slug dùng cho việc tạo friendly url.
 *
 * @access    public
 * @param    string
 * @return    string
 */
if (!function_exists('currency_format')) {
    function currency_format($number, $suffix = 'đ'): string
    {
        return number_format($number, 0, ',', '.') . (string)($suffix);
    }
}


/**
 *
 * Hiển thị thời gian: Hôm nay, 1 ngày trước, 1 ngày sau ...
 * ex: https://carbon.nesbot.com/docs/#api-humandiff
 *
 * @access    public
 * @param    string
 * @return    string
 */
if (!function_exists('date_for_humans')) {
    function date_for_humans($fromDate, $toDate): string
    {
        $toDate     = date_format(date_create($toDate), 'Y-m-d');
        $fromDate   = date_format(date_create($fromDate), 'Y-m-d');
        if ($fromDate === $toDate) {
            return 'Hôm nay';
        }
        return Carbon::parse($fromDate)->diffForHumans($toDate);
    }
}
