<?php
namespace App\Blade;

use Illuminate\Support\Facades\Route;

class Directives
{
    public function register(): array
    {
        return [
            /*
            |---------------------------------------------------------------------
            | @showMenuBackend @endif
            |---------------------------------------------------------------------
            |
            | Hiển thị menu hoặc không dựa vào biến $showMenuBackend từ shareView
            |
            */
            'showMenuBackend' => function ($status = true) {
                return $status === true;
            },

            /*
            |---------------------------------------------------------------------
            | @isHomeBackend @endif
            |---------------------------------------------------------------------
            |
            | Kiểm tra có phải trang homepage (backend) hay không.
            |
            */
            'isHomeBackend' => function () {
                return in_array(Route::getCurrentRoute()->getName(), ['dashboard-backend', 'dashboard-backend-2']);
            },
        ];
    }
}
