<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
class Kernel extends ConsoleKernel
{
    protected $commands = [
        Commands\RemoveSession::class,
        Commands\DbBackup::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        $emails = config('constants.schedule_send_email');

        /*
        |--------------------------------------------------------------------------
        | Cron by daily
        |--------------------------------------------------------------------------
        */

        /*
        |--------------------------------------------------------------------------
        | Cron by weekly
        |--------------------------------------------------------------------------
        */
        $schedule
            ->command('session:remove-file')
            ->weekly()
            ->withoutOverlapping()
            ->runInBackground()
            ->emailOutputOnFailure($emails)
            ->emailOutputTo($emails)
            ->emailWrittenOutputTo($emails)
            ->evenInMaintenanceMode()
            ->before(function () {
                Log::notice('Start :: session:remove-file (CRON)');
            })
            ->after(function () {
                Log::info('End :: session:remove-file (CRON)');
            })
            ;

        $schedule
            ->command('database:backup')
            ->weekly()
            ->withoutOverlapping()
            ->runInBackground()
            ->emailOutputOnFailure($emails)
            ->emailOutputTo($emails)
            ->emailWrittenOutputTo($emails)
            ->evenInMaintenanceMode()
            ->before(function () {
                Log::notice('Start :: database:backup (CRON)');
            })
            ->after(function () {
                Log::info('End :: database:backup (CRON)');
            });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
