<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class DbBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Database Backup';

    /**
     * @return void
     */
    public function handle(): void
    {
        create_folder(storage_path('app/'), 'backup');

        // Xóa file lớn hơn 30 ngày
        $files = get_files_in_folder(storage_path('app/backup'));
        delete_files_older_than_days($files, 30);

        $filename   = "backup-" . Carbon::now()->format('Y-m-d') . ".sql";
        $command    = "mysqldump --user=" . getenv('DB_USERNAME') . " --password=" . getenv('DB_PASSWORD') . " --host=" . getenv('DB_HOST') . " " . getenv('DB_DATABASE') . "  > " . storage_path() . "/app/backup/" . $filename;
        $returnVar  = NULL;
        $output     = NULL;
        exec($command, $output, $returnVar);
    }
}
