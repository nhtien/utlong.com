<?php

namespace App\Providers;

use App\Models\Menu;
use App\Models\User;
use App\Rules\CheckNodeStatus;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    protected array $rules = [
        \App\Rules\Username::class,
        \App\Rules\PhoneNumber::class,
        \App\Rules\Password::class,
        \App\Rules\DoNotContainHtmlTag::class,
        \App\Rules\DoNotContainMultipleWhitespace::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind('UserModel', User::class);
        $this->app->bind('MenuModel', Menu::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        /**
         * Register rule
         */
        $this->registerValidationRules();
        $this->registerValidationRulesWithParameters();

        /**
         * share view
         */
        $this->shareView();

        /**
         * Create throttle middleware
         */
        RateLimiter::for('uploads', function (Request $request) {
            return $request->input('allow')
                ? Limit::none()
                : Limit::perMinute(1)->by($request->ip());
        });
    }

    /**
     * @return void
     */
    private function registerValidationRulesWithParameters(): void
    {
        Validator::extend('check_node_status', function ($attribute, $value, $parameters, $validator){
            list($modelName, $nodeId, $status) = $parameters;
            return (new CheckNodeStatus($modelName, $nodeId, $status))->passes($attribute, $value);
        });
    }

    /**
     * @return void
     */
    private function registerValidationRules(): void
    {
        foreach($this->rules as $class ) {
            $alias = (new $class)->__toString();
            if ($alias) {
                Validator::extend($alias, $class .'@passes');
            }
        }
    }


    public function shareView()
    {
        View::share('showMenuBackend', true);
    }

}
