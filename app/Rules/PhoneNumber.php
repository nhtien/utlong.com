<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneNumber implements Rule
{
    /**
     * @var string
     */
    protected string $alias = 'phone_number';

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->alias;
    }

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value    A
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return preg_match('/^(070|079|077|076|078|089|090|093|083|084|085|081|082|088|091|094|032|033|034|035|036|037|038|039|086|096|097|098|056|058|092|059|099)\d{7}/u', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.phone_number');
    }
}
