<?php
namespace App\Helpers;

class Order
{
    protected mixed $farmer = null;

    private int $maxDiscountFee = 20000; // số tiền khuyến mãi tối đa có thể dùng


    /**
     * @param array $yourMoneys
     * @param float $totalFee
     * @return array
     */
    public function totalEstimatedCost(array $yourMoneys, float $totalFee): array
    {
        $result = [
            'allowance' => 0,
            'wallet'    => 0, // phí thực tế phải trả
        ];
        $yourAllowance = max($yourMoneys['allowance'], 0);

        // Tiền thực khuyến mãi thực tế sẽ giảm
        $discountFeeCurrent = min($this->maxDiscountFee, $yourAllowance);

        if ($totalFee <= $discountFeeCurrent) {
            $result['allowance'] = $totalFee;
        }else{
            $result['allowance'] = $discountFeeCurrent;
            $result['wallet'] = $totalFee - $discountFeeCurrent;
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getMaxDiscountFee(): int
    {
        return $this->maxDiscountFee;
    }

}
