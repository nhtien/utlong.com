<?php

namespace App\Listeners;

use App\Models\ModelLog;

class WriteModelLogListener
{
    /**
     * @var ModelLog
     */
    private ModelLog $modelLog;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ModelLog $modelLog)
    {
        $this->modelLog = $modelLog;
    }

    /**
     * @param object $event
     * @return void
     */
    public function handle(object $event): void
    {
        if ($event->model) {
            $data = [
                        'table_name' => $event->model->getTable(),
                        'content'    => $event->model->getAttributes(),
                        'type'       => $event->eventName ,
                        'user_id'    => get_user_id()
                    ];
            $this->modelLog::query()->create($data);
        }
    }
}
