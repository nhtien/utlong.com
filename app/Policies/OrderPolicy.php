<?php

namespace App\Policies;

use App\Models\Machine;
use App\Models\Order;
use App\Models\Farmer;

class OrderPolicy
{
    /**
     * @param Farmer $user
     * @param Order $order
     * @return bool
     */
    public function updateOrderFarmer(Farmer $user, Order $order): bool
    {
        return $user->farmer_id == $order->farmer_id;
    }

    /**
     * @param Machine $user
     * @param Order $order
     * @return bool
     */
    public function updateOrderMachine(Machine $user, Order $order): bool
    {
        return $user->machine_id == $order->machine_id;
    }
}
