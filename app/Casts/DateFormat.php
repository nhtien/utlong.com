<?php

namespace App\Casts;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;

/**
 * https://carbon.nesbot.com/docs/#api-localization
 */
class DateFormat implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, string $key, $value, array $attributes): mixed
    {
        $carbon = new Carbon($value);
        $carbon->settings([
            'locale'    => app()->getLocale(),
            'timezone'  => config('app.timezone'),
        ]);
        return $carbon->isoFormat('DD-MM-YYYY'); //'DD-MM-YYYY HH:mm'
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, string $key, $value, array $attributes): mixed
    {
        return $value;
    }
}
